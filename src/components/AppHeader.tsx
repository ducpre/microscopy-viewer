import React from "react";
import {
  Col,
  Descriptions,
  Input,
  Layout,
  Modal,
  Row,
  Space,
  Badge,
  Collapse,
} from "antd";
import {
  CheckOutlined,
  InfoOutlined,
  StopOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { detect } from "detect-browser";

import Button from "./Button";
import { RouteComponentProps, withRouter } from "../utils/router";
import NotificationMiddleware, {
  NotificationMiddlewareEvents,
} from "../services/NotificationMiddleware";
import { CustomError } from "../utils/CustomError";
import { v4 as uuidv4 } from "uuid";
import Typography from "antd/lib/typography/Typography";

interface HeaderProps extends RouteComponentProps {
  app: {
    name: string;
    version: string;
    homepage: string;
    uid: string;
    organization?: string;
  };
}

interface HeaderState {
  selectedServerUrl?: string;
  isServerSelectionModalVisible: boolean;
  isServerSelectionDisabled: boolean;
  errorObj: CustomError[];
  errorCategory: string[];
}

/**
 * React component for the application header.
 */
class AppHeader extends React.Component<HeaderProps, HeaderState> {
  constructor(props: HeaderProps) {
    super(props);
    this.state = {
      isServerSelectionModalVisible: false,
      isServerSelectionDisabled: true,
      errorObj: [],
      errorCategory: [],
    };

    const onErrorHandler = ({
      error,
    }: {
      category: string;
      error: CustomError;
    }): void => {
      this.setState({
        errorObj: [...this.state.errorObj, error],
        errorCategory: [...this.state.errorCategory, error.type],
      });
    };

    NotificationMiddleware.subscribe(
      NotificationMiddlewareEvents.OnError,
      onErrorHandler
    );
  }

  handleInfoButtonClick = (): void => {
    const browser = detect();
    const environment: {
      browser: {
        name?: string;
        version?: string;
      };
      os: {
        name?: string;
      };
    } = {
      browser: {},
      os: {},
    };
    if (browser != null) {
      environment.browser = {
        name: browser.name != null ? browser.name : undefined,
        version: browser.version != null ? browser.version : undefined,
      };
      environment.os = {
        name: browser.os != null ? browser.os : undefined,
      };
    }

    Modal.info({
      title: "About",
      width: 600,
      content: (
        <>
          <Descriptions title="Application" column={1}>
            <Descriptions.Item label="Name">
              {this.props.app.name}
            </Descriptions.Item>
            <Descriptions.Item label="Version">
              {this.props.app.version}
            </Descriptions.Item>
            <Descriptions.Item label="Homepage">
              {this.props.app.homepage}
            </Descriptions.Item>
          </Descriptions>
          <Descriptions title="Browser" column={1}>
            <Descriptions.Item label="Name">
              {environment.browser.name}
            </Descriptions.Item>
            <Descriptions.Item label="Version">
              {environment.browser.version}
            </Descriptions.Item>
          </Descriptions>
          <Descriptions title="Operating System" column={1}>
            <Descriptions.Item label="Name">
              {environment.os.name}
            </Descriptions.Item>
          </Descriptions>
        </>
      ),
      onOk(): void {},
    });
  };

  handleDebugButtonClick = (): void => {
    const errorMsgs: {
      Authentication: string[];
      Communication: string[];
      EncodingDecoding: string[];
      Visualization: string[];
    } = {
      Authentication: [],
      Communication: [],
      EncodingDecoding: [],
      Visualization: [],
    };

    type ObjectKey = keyof typeof errorMsgs;
    const errorNum = this.state.errorObj.length;

    if (errorNum > 0) {
      for (let i = 0; i < errorNum; i++) {
        const category = this.state.errorCategory[i] as ObjectKey;
        errorMsgs[category].push(this.state.errorObj[i].message);
      }
    }

    const { Panel } = Collapse;

    const showErrorCount = (errcount: number): JSX.Element => (
      <Badge count={errcount} />
    );

    Modal.info({
      title: "Debug Information\n (Check console for more information)",
      width: 800,
      content: (
        <Collapse>
          <Panel
            header="Communication Error"
            key="communicationerror"
            extra={showErrorCount(errorMsgs.Communication.length)}
          >
            <ol>
              {errorMsgs.Communication.map((e) => (
                <li key={uuidv4()}>{e}</li>
              ))}
            </ol>
          </Panel>
          <Panel
            header="Data Encoding/Decoding error"
            key="encodedecodeerror"
            extra={showErrorCount(errorMsgs.EncodingDecoding.length)}
          >
            <ol>
              {errorMsgs.EncodingDecoding.map((e) => (
                <li key={uuidv4()}>{e}</li>
              ))}
            </ol>
          </Panel>
          <Panel
            header="Visualization error"
            key="visualizationerror"
            extra={showErrorCount(errorMsgs.Visualization.length)}
          >
            <ol>
              {errorMsgs.Visualization.map((e) => (
                <li key={uuidv4()}>{e}</li>
              ))}
            </ol>
          </Panel>
          <Panel
            header="Authentication error"
            key="autherror"
            extra={showErrorCount(errorMsgs.Authentication.length)}
          >
            <ol>
              {errorMsgs.Authentication.map((e) => (
                <li key={uuidv4()}>{e}</li>
              ))}
            </ol>
          </Panel>
        </Collapse>
      ),
      onOk(): void {},
    });
  };

  handleServerSelectionButtonClick = (): void => {
    this.setState({ isServerSelectionModalVisible: true });
  };

  render(): React.ReactNode {
    const infoButton = (
      <Button
        icon={InfoOutlined}
        tooltip="Get app info"
        onClick={this.handleInfoButtonClick}
      />
    );

    const debugButton = (
      <Badge count={this.state.errorObj.length}>
        <Button
          icon={SettingOutlined}
          tooltip="Debug info"
          onClick={this.handleDebugButtonClick}
        />
      </Badge>
    );

    const handleServerSelectionInput = (
      event: React.FormEvent<HTMLInputElement>
    ): void => {
      const value = event.currentTarget.value;
      let isDisabled = true;
      if (value != null) {
        try {
          const url = new URL(value);
          if (url.protocol.startsWith("http") && url.pathname.length > 0) {
            isDisabled = false;
          }
        } catch (TypeError) {}
      }
      this.setState({
        selectedServerUrl: value,
        isServerSelectionDisabled: isDisabled,
      });
    };

    const handleServerSelectionCancellation = (event: any): void => {
      this.setState({
        selectedServerUrl: undefined,
        isServerSelectionModalVisible: false,
        isServerSelectionDisabled: true,
      });
    };

    return (
      <>
        <Layout.Header
          style={{
            width: "100%",
            background: "#383333",
            height: "30px",
            padding: "0 20px",
          }}
        >
          <Row>
            <Col style={{ lineHeight: "28px" }}>
              <Typography
                style={{ fontSize: "20px", color: "#078ae5" }}
              ></Typography>
            </Col>
            <Col flex="auto"></Col>
            <Col style={{ lineHeight: 0 }}>
              <Space direction="horizontal">
                {infoButton}
                {debugButton}
              </Space>
            </Col>
          </Row>
        </Layout.Header>

        <Modal
          visible={this.state.isServerSelectionModalVisible}
          title="Select DICOMweb server"
          onCancel={handleServerSelectionCancellation}
        >
          <Input
            placeholder="Enter base URL of DICOMweb Study Service"
            onChange={handleServerSelectionInput}
            addonAfter={
              this.state.isServerSelectionDisabled ? (
                <StopOutlined style={{ color: "rgba(0,0,0,.45)" }} />
              ) : (
                <CheckOutlined style={{ color: "rgba(0,0,0,.45)" }} />
              )
            }
          />
        </Modal>
      </>
    );
  }
}

export default withRouter(AppHeader);
