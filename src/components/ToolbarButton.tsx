import React from "react";
import { Button as Btn, Divider, Tooltip } from "antd";

interface ButtonProps {
  icon?: any;
  tooltip?: string;
  label?: string;
  onClick?: (options: any) => void;
  isSelected?: boolean;
  svgIcon?: React.ReactNode;
  disabled?: boolean;
}

/**
 * React component for a button.
 */
class ToolbarButton extends React.Component<ButtonProps, {}> {
  constructor(props: ButtonProps) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(event: React.SyntheticEvent): void {
    if (this.props.onClick !== undefined) {
      this.props.onClick(event);
    }
  }

  render(): React.ReactNode {
    const Icon = this.props.icon;
    const SvgIcon = this.props.svgIcon;
    // if (Icon === undefined) {
    //   return null;
    // }

    let text;
    if (this.props.label != null) {
      text = (
        <>
          <Divider type="vertical" />
          {this.props.label}
        </>
      );
    }

    let button;

    button = (
      <Btn
        onClick={this.handleClick}
        icon={
          Icon ? (
            <div style={{ fontSize: "20px" }}>
              <Icon />
            </div>
          ) : (
            <div style={{ fontSize: "10px" }}>{SvgIcon}</div>
          )
        }
        disabled={this.props.disabled}
        type={this.props.isSelected ? "primary" : "default"}
        style={{
          lineHeight: "1.0",
          width: "40px",
          height: "40px",
          border: "none",
        }}
      >
        {text}
      </Btn>
    );

    if (this.props.tooltip !== undefined) {
      return <Tooltip title={this.props.tooltip}>{button}</Tooltip>;
    } else {
      return button;
    }
  }
}

export default ToolbarButton;
