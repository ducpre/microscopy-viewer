import React from "react";
import {
  FaCrosshairs,
  FaDrawPolygon,
  FaEye,
  FaEyeSlash,
  FaHandPaper,
  FaHandPointer,
  FaTrash,
  FaSave,
} from "react-icons/fa";
import {
  Button as Btn,
  Checkbox,
  Descriptions,
  Divider,
  InputNumber,
  message,
  Menu,
  Modal,
  Layout,
  Row,
  Select,
  Space,
  Tooltip,
  Typography,
} from "antd";
import { UndoOutlined, CheckOutlined, StopOutlined } from "@ant-design/icons";
import * as dmv from "dicom-microscopy-viewer";
import * as dcmjs from "dcmjs";
import * as dwc from "dicomweb-client";

import DicomWebManager from "../DicomWebManager";
import AnnotationList from "./AnnotationList";
import AnnotationGroupList from "./AnnotationGroupList";
import Button from "./Button";
import Equipment from "./Equipment";
import Report, { MeasurementReport } from "./Report";
import SpecimenList from "./SpecimenList";
import OpticalPathList from "./OpticalPathList";
import MappingList from "./MappingList";
import SegmentList from "./SegmentList";
import { AnnotationSettings } from "../AppConfig";
import { Slide } from "../data/slides";
import { StorageClasses } from "../data/uids";
import { findContentItemsByName } from "../utils/sr";
import { RouteComponentProps, withRouter } from "../utils/router";
import { CustomError, errorTypes } from "../utils/CustomError";
import NotificationMiddleware, {
  NotificationMiddlewareContext,
} from "../services/NotificationMiddleware";
import Header from "./Header";
import ToolbarButton from "./ToolbarButton";

const DEFAULT_ROI_STROKE_COLOR: number[] = [0, 126, 163];
const DEFAULT_ROI_FILL_COLOR: number[] = [0, 126, 163, 0.2];
const DEFAULT_ROI_STROKE_WIDTH: number = 2;
const DEFAULT_ROI_RADIUS: number = 5;

const _buildKey = (concept: {
  CodeValue: string;
  CodeMeaning: string;
  CodingSchemeDesignator: string;
  CodingSchemeVersion?: string;
}): string => {
  const codingScheme = concept.CodingSchemeDesignator;
  const codeValue = concept.CodeValue;
  return `${codingScheme}-${codeValue}`;
};

const _getRoiKey = (roi: dmv.roi.ROI): string | undefined => {
  const matches = findContentItemsByName({
    content: roi.evaluations,
    name: new dcmjs.sr.coding.CodedConcept({
      value: "121071",
      meaning: "Finding",
      schemeDesignator: "DCM",
    }),
  });
  if (matches.length === 0) {
    console.warn(`no finding found for ROI ${roi.uid}`);
    return;
  }
  const finding = matches[0] as dcmjs.sr.valueTypes.CodeContentItem;
  const findingName = finding.ConceptCodeSequence[0];
  return _buildKey(findingName);
};

const _areROIsEqual = (a: dmv.roi.ROI, b: dmv.roi.ROI): boolean => {
  if (a.scoord3d.graphicType !== b.scoord3d.graphicType) {
    return false;
  }
  if (a.scoord3d.frameOfReferenceUID !== b.scoord3d.frameOfReferenceUID) {
    return false;
  }
  if (a.scoord3d.graphicData.length !== b.scoord3d.graphicData.length) {
    return false;
  }

  const decimals = 6;
  for (let i = 0; i < a.scoord3d.graphicData.length; ++i) {
    if (a.scoord3d.graphicType === "POINT") {
      const s1 = a.scoord3d as dmv.scoord3d.Point;
      const s2 = b.scoord3d as dmv.scoord3d.Point;
      const c1 = s1.graphicData[i].toPrecision(decimals);
      const c2 = s2.graphicData[i].toPrecision(decimals);
      if (c1 !== c2) {
        return false;
      }
    } else {
      const s1 = a.scoord3d as dmv.scoord3d.Polygon;
      const s2 = b.scoord3d as dmv.scoord3d.Polygon;
      for (let j = 0; j < s1.graphicData[i].length; ++j) {
        const c1 = s1.graphicData[i][j].toPrecision(decimals);
        const c2 = s2.graphicData[i][j].toPrecision(decimals);
        if (c1 !== c2) {
          return false;
        }
      }
    }
  }
  return true;
};

const _formatRoiStyle = (style: {
  stroke?: {
    color?: number[];
    width?: number;
  };
  fill?: {
    color?: number[];
  };
  radius?: number;
}): dmv.viewer.ROIStyleOptions => {
  const stroke = {
    color: DEFAULT_ROI_STROKE_COLOR,
    width: DEFAULT_ROI_STROKE_WIDTH,
  };
  if (style.stroke != null) {
    if (style.stroke.color != null) {
      stroke.color = style.stroke.color;
    }
    if (style.stroke.width != null) {
      stroke.width = style.stroke.width;
    }
  }
  const fill = {
    color: DEFAULT_ROI_FILL_COLOR,
  };
  if (style.fill != null) {
    if (style.fill.color != null) {
      fill.color = style.fill.color;
    }
  }
  return {
    stroke,
    fill,
    image: {
      circle: {
        radius:
          style.radius != null ? style.radius : Math.max(5 - stroke.width, 1),
        stroke,
        fill,
      },
    },
  };
};

const _constructViewers = ({
  clients,
  slide,
  preload,
}: {
  clients: { [key: string]: dwc.api.DICOMwebClient };
  slide: Slide;
  preload?: boolean;
}): {
  volumeViewer: dmv.viewer.VolumeImageViewer;
  labelViewer?: dmv.viewer.LabelImageViewer;
} => {
  console.info(
    "instantiate viewer for VOLUME images of slide " +
      `"${slide.volumeImages[0].ContainerIdentifier}"`
  );
  try {
    const volumeViewer = new dmv.viewer.VolumeImageViewer({
      clientMapping: clients,
      metadata: slide.volumeImages,
      controls: ["overview", "position"],
      preload: preload,
      errorInterceptor: (error: CustomError) =>
        NotificationMiddleware.onError(
          NotificationMiddlewareContext.DMV,
          error
        ),
    });
    volumeViewer.activateSelectInteraction({});

    let labelViewer;
    if (slide.labelImages.length > 0) {
      console.info(
        "instantiate viewer for LABEL image of slide " +
          `"${slide.labelImages[0].ContainerIdentifier}"`
      );
      labelViewer = new dmv.viewer.LabelImageViewer({
        client: clients[StorageClasses.VL_WHOLE_SLIDE_MICROSCOPY_IMAGE],
        metadata: slide.labelImages[0],
        resizeFactor: 1,
        orientation: "vertical",
        errorInterceptor: (error: CustomError) =>
          NotificationMiddleware.onError(
            NotificationMiddlewareContext.DMV,
            error
          ),
      });
    }

    return { volumeViewer, labelViewer };
  } catch (error) {
    // eslint-disable-next-line @typescript-eslint/no-floating-promises
    NotificationMiddleware.onError(
      NotificationMiddlewareContext.SLIM,
      new CustomError(errorTypes.VISUALIZATION, "Failed to instantiate viewer")
    );
    throw error;
  }
};

/*
 * Check whether the report is structured according to template
 * TID 1500 "MeasurementReport".
 */
const _implementsTID1500 = (
  report: dmv.metadata.Comprehensive3DSR
): boolean => {
  const templateSeq = report.ContentTemplateSequence;
  if (templateSeq.length > 0) {
    const tid = templateSeq[0].TemplateIdentifier;
    if (tid === "1500") {
      return true;
    }
  }
  return false;
};

/*
 * Check whether the subject described in the report is a specimen as compared
 * to a patient, fetus, or device.
 */
const _describesSpecimenSubject = (
  report: dmv.metadata.Comprehensive3DSR
): boolean => {
  const items = findContentItemsByName({
    content: report.ContentSequence,
    name: new dcmjs.sr.coding.CodedConcept({
      value: "121024",
      schemeDesignator: "DCM",
      meaning: "Subject Class",
    }),
  });
  if (items.length === 0) {
    return false;
  }
  const subjectClassItem = items[0] as dcmjs.sr.valueTypes.CodeContentItem;
  const subjectClassValue = subjectClassItem.ConceptCodeSequence[0];
  const retrievedConcept = new dcmjs.sr.coding.CodedConcept({
    value: subjectClassValue.CodeValue,
    meaning: subjectClassValue.CodeMeaning,
    schemeDesignator: subjectClassValue.CodingSchemeDesignator,
  });
  const expectedConcept = new dcmjs.sr.coding.CodedConcept({
    value: "121027",
    meaning: "Specimen",
    schemeDesignator: "DCM",
  });
  if (retrievedConcept.equals(expectedConcept)) {
    return true;
  }
  return false;
};

/*
 * Check whether the report contains appropriate graphic ROI annotations.
 */
const _containsROIAnnotations = (
  report: dmv.metadata.Comprehensive3DSR
): boolean => {
  const measurements = findContentItemsByName({
    content: report.ContentSequence,
    name: new dcmjs.sr.coding.CodedConcept({
      value: "126010",
      schemeDesignator: "DCM",
      meaning: "Imaging Measurements",
    }),
  });
  if (measurements.length === 0) {
    return false;
  }
  const container = measurements[0] as dcmjs.sr.valueTypes.ContainerContentItem;
  const measurementGroups = findContentItemsByName({
    content: container.ContentSequence,
    name: new dcmjs.sr.coding.CodedConcept({
      value: "125007",
      schemeDesignator: "DCM",
      meaning: "Measurement Group",
    }),
  });

  let foundRegion = false;
  measurementGroups.forEach((group) => {
    const container = group as dcmjs.sr.valueTypes.ContainerContentItem;
    const regions = findContentItemsByName({
      content: container.ContentSequence,
      name: new dcmjs.sr.coding.CodedConcept({
        value: "111030",
        schemeDesignator: "DCM",
        meaning: "Image Region",
      }),
    });
    if (regions.length > 0) {
      if (regions[0].ValueType === dcmjs.sr.valueTypes.ValueTypes.SCOORD3D) {
        foundRegion = true;
      }
    }
  });

  return foundRegion;
};

interface EvaluationOptions {
  name: dcmjs.sr.coding.CodedConcept;
  values: dcmjs.sr.coding.CodedConcept[];
}

interface Evaluation {
  name: dcmjs.sr.coding.CodedConcept;
  value: dcmjs.sr.coding.CodedConcept;
}

interface Measurement {
  name: dcmjs.sr.coding.CodedConcept;
  value?: number;
  unit: dcmjs.sr.coding.CodedConcept;
}

interface SlideViewerProps extends RouteComponentProps {
  slide: Slide;
  clients: { [key: string]: DicomWebManager };
  studyInstanceUID: string;
  seriesInstanceUID: string;
  preload?: boolean;
  annotations: AnnotationSettings[];
  enableAnnotationTools: boolean;
  user?: {
    name: string;
    email: string;
  };
  selectedPresentationStateUID?: string;
  appInfo: {
    name: string;
    version: string;
    homepage: string;
    uid: string;
    organization: string | undefined;
  };
  LayoutSider: JSX.Element;
}

interface SlideViewerState {
  visibleRoiUIDs: Set<string>;
  visibleSegmentUIDs: Set<string>;
  visibleMappingUIDs: Set<string>;
  visibleAnnotationGroupUIDs: Set<string>;
  visibleOpticalPathIdentifiers: Set<string>;
  activeOpticalPathIdentifiers: Set<string>;
  presentationStates: dmv.metadata.AdvancedBlendingPresentationState[];
  selectedPresentationStateUID?: string;
  selectedFinding?: dcmjs.sr.coding.CodedConcept;
  selectedEvaluations: Evaluation[];
  selectedGeometryType?: string;
  selectedMarkup?: string;
  selectedRoi?: dmv.roi.ROI;
  selectedRoiUIDs: Set<string>;
  generatedReport?: dmv.metadata.Comprehensive3DSR;
  isLoading: boolean;
  isAnnotationModalVisible: boolean;
  isSelectedRoiModalVisible: boolean;
  isReportModalVisible: boolean;
  isRoiDrawingActive: boolean;
  isRoiModificationActive: boolean;
  isRoiTranslationActive: boolean;
  isGoToModalVisible: boolean;
  isSelectedMagnificationValid: boolean;
  isSelectedXCoordinateValid: boolean;
  isSelectedYCoordinateValid: boolean;
  selectedXCoordinate?: number;
  validXCoordinateRange: number[];
  selectedYCoordinate?: number;
  validYCoordinateRange: number[];
  selectedMagnification?: number;
  areRoisHidden: boolean;
  pixelDataStatistics: {
    [opticalPathIdentifier: string]: {
      min: number;
      max: number;
      numFramesSampled: number;
    };
  };
  loadingFrames: Set<string>;
}

/**
 * React component for interactive viewing of an individual digital slide,
 * which corresponds to one DICOM Series of DICOM Slide Microscopy images and
 * potentially one or more associated DICOM Series of DICOM SR documents.
 */
class SlideViewer extends React.Component<SlideViewerProps, SlideViewerState> {
  private readonly findingOptions: dcmjs.sr.coding.CodedConcept[] = [];

  private readonly evaluationOptions: { [key: string]: EvaluationOptions[] } =
    {};

  private readonly measurements: Measurement[] = [];

  private readonly geometryTypeOptions: { [key: string]: string[] } = {};

  private readonly volumeViewportRef: React.RefObject<HTMLDivElement>;

  private readonly labelViewportRef: React.RefObject<HTMLDivElement>;

  private volumeViewer: dmv.viewer.VolumeImageViewer;

  private labelViewer?: dmv.viewer.LabelImageViewer;

  private readonly defaultRoiStyle: dmv.viewer.ROIStyleOptions = {
    stroke: {
      color: DEFAULT_ROI_STROKE_COLOR,
      width: DEFAULT_ROI_STROKE_WIDTH,
    },
    fill: {
      color: DEFAULT_ROI_FILL_COLOR,
    },
    image: {
      circle: {
        fill: {
          color: DEFAULT_ROI_STROKE_COLOR,
        },
        radius: DEFAULT_ROI_RADIUS,
      },
    },
  };

  private roiStyles: { [key: string]: dmv.viewer.ROIStyleOptions } = {};

  private readonly selectionColor: number[] = [140, 184, 198];

  private readonly selectedRoiStyle: dmv.viewer.ROIStyleOptions = {
    stroke: { color: [...this.selectionColor, 1], width: 3 },
    fill: { color: [...this.selectionColor, 0.2] },
    image: {
      circle: {
        radius: 5,
        fill: { color: [...this.selectionColor, 1] },
      },
    },
  };

  constructor(props: SlideViewerProps) {
    super(props);
    console.info(
      `view slide "${this.props.slide.containerIdentifier}": `,
      this.props.slide
    );
    const geometryTypeOptions = [
      "point",
      "circle",
      "box",
      "polygon",
      "line",
      "freehandpolygon",
      "freehandline",
    ];
    props.annotations.forEach((annotation: AnnotationSettings) => {
      const finding = new dcmjs.sr.coding.CodedConcept(annotation.finding);
      this.findingOptions.push(finding);
      const key = _buildKey(finding);
      if (annotation.geometryTypes !== undefined) {
        this.geometryTypeOptions[key] = annotation.geometryTypes;
      } else {
        this.geometryTypeOptions[key] = geometryTypeOptions;
      }
      this.evaluationOptions[key] = [];
      if (annotation.evaluations !== undefined) {
        annotation.evaluations.forEach((evaluation) => {
          this.evaluationOptions[key].push({
            name: new dcmjs.sr.coding.CodedConcept(evaluation.name),
            values: evaluation.values.map((value) => {
              return new dcmjs.sr.coding.CodedConcept(value);
            }),
          });
        });
      }
      if (annotation.measurements !== undefined) {
        annotation.measurements.forEach((measurement) => {
          this.measurements.push({
            name: new dcmjs.sr.coding.CodedConcept(measurement.name),
            value: undefined,
            unit: new dcmjs.sr.coding.CodedConcept(measurement.unit),
          });
        });
      }
      if (annotation.style != null) {
        this.roiStyles[key] = _formatRoiStyle(annotation.style);
      } else {
        this.roiStyles[key] = this.defaultRoiStyle;
      }
    });

    this.componentSetup = this.componentSetup.bind(this);
    this.componentCleanup = this.componentCleanup.bind(this);

    this.onWindowResize = this.onWindowResize.bind(this);
    this.handleRoiDrawing = this.handleRoiDrawing.bind(this);
    this.handleRoiTranslation = this.handleRoiTranslation.bind(this);
    this.handleRoiModification = this.handleRoiModification.bind(this);
    this.handleRoiVisibilityChange = this.handleRoiVisibilityChange.bind(this);
    this.handleRoiRemoval = this.handleRoiRemoval.bind(this);
    this.handleRoiSelectionCancellation =
      this.handleRoiSelectionCancellation.bind(this);
    this.handleAnnotationConfigurationCancellation =
      this.handleAnnotationConfigurationCancellation.bind(this);
    this.handleAnnotationGeometryTypeSelection =
      this.handleAnnotationGeometryTypeSelection.bind(this);
    this.handleAnnotationMeasurementActivation =
      this.handleAnnotationMeasurementActivation.bind(this);
    this.handleAnnotationFindingSelection =
      this.handleAnnotationFindingSelection.bind(this);
    this.handleAnnotationEvaluationSelection =
      this.handleAnnotationEvaluationSelection.bind(this);
    this.handleAnnotationEvaluationClearance =
      this.handleAnnotationEvaluationClearance.bind(this);
    this.handleAnnotationConfigurationCompletion =
      this.handleAnnotationConfigurationCompletion.bind(this);
    this.handleAnnotationSelection = this.handleAnnotationSelection.bind(this);
    this.handleAnnotationVisibilityChange =
      this.handleAnnotationVisibilityChange.bind(this);
    this.handleAnnotationGroupVisibilityChange =
      this.handleAnnotationGroupVisibilityChange.bind(this);
    this.handleAnnotationGroupStyleChange =
      this.handleAnnotationGroupStyleChange.bind(this);
    this.handleGoTo = this.handleGoTo.bind(this);
    this.handleXCoordinateSelection =
      this.handleXCoordinateSelection.bind(this);
    this.handleYCoordinateSelection =
      this.handleYCoordinateSelection.bind(this);
    this.handleMagnificationSelection =
      this.handleMagnificationSelection.bind(this);
    this.handleSlidePositionSelection =
      this.handleSlidePositionSelection.bind(this);
    this.handleSlidePositionSelectionCancellation =
      this.handleSlidePositionSelectionCancellation.bind(this);
    this.handleReportGeneration = this.handleReportGeneration.bind(this);
    this.handleReportVerification = this.handleReportVerification.bind(this);
    this.handleReportCancellation = this.handleReportCancellation.bind(this);
    this.handleSegmentVisibilityChange =
      this.handleSegmentVisibilityChange.bind(this);
    this.handleSegmentStyleChange = this.handleSegmentStyleChange.bind(this);
    this.handleMappingVisibilityChange =
      this.handleMappingVisibilityChange.bind(this);
    this.handleMappingStyleChange = this.handleMappingStyleChange.bind(this);
    this.handleOpticalPathVisibilityChange =
      this.handleOpticalPathVisibilityChange.bind(this);
    this.handleOpticalPathStyleChange =
      this.handleOpticalPathStyleChange.bind(this);
    this.handleOpticalPathActivityChange =
      this.handleOpticalPathActivityChange.bind(this);
    this.handlePresentationStateSelection =
      this.handlePresentationStateSelection.bind(this);
    this.handlePresentationStateReset =
      this.handlePresentationStateReset.bind(this);

    const { volumeViewer, labelViewer } = _constructViewers({
      clients: this.props.clients,
      slide: this.props.slide,
      preload: this.props.preload,
    });
    this.volumeViewer = volumeViewer;
    this.labelViewer = labelViewer;
    this.volumeViewportRef = React.createRef<HTMLDivElement>();
    this.labelViewportRef = React.createRef<HTMLDivElement>();

    /**
     * Deactivate all optical paths. Visibility will be set later, potentially
     * using based on available presentation state instances.
     */
    this.volumeViewer.getAllOpticalPaths().forEach((opticalPath) => {
      this.volumeViewer.deactivateOpticalPath(opticalPath.identifier);
    });

    const [offset, size] = this.volumeViewer.boundingBox;

    this.state = {
      selectedRoiUIDs: new Set(),
      visibleRoiUIDs: new Set(),
      visibleSegmentUIDs: new Set(),
      visibleMappingUIDs: new Set(),
      visibleAnnotationGroupUIDs: new Set(),
      visibleOpticalPathIdentifiers: new Set(),
      activeOpticalPathIdentifiers: new Set(),
      presentationStates: [],
      selectedFinding: undefined,
      selectedEvaluations: [],
      generatedReport: undefined,
      isLoading: false,
      isAnnotationModalVisible: false,
      isSelectedRoiModalVisible: false,
      isSelectedMagnificationValid: false,
      isReportModalVisible: false,
      isRoiDrawingActive: false,
      isRoiTranslationActive: false,
      isRoiModificationActive: false,
      isGoToModalVisible: false,
      isSelectedXCoordinateValid: false,
      isSelectedYCoordinateValid: false,
      selectedXCoordinate: undefined,
      validXCoordinateRange: [offset[0], offset[0] + size[0]],
      selectedYCoordinate: undefined,
      validYCoordinateRange: [offset[1], offset[1] + size[1]],
      selectedMagnification: undefined,
      areRoisHidden: false,
      pixelDataStatistics: {},
      selectedPresentationStateUID: this.props.selectedPresentationStateUID,
      loadingFrames: new Set(),
    };
  }

  componentDidUpdate(
    previousProps: SlideViewerProps,
    previousState: SlideViewerState
  ): void {
    /** Fetch data and update the viewports if the route has changed (
     * i.e., if another series has been selected) or if the client has changed.
     */
    if (
      this.props.location.pathname !== previousProps.location.pathname ||
      this.props.studyInstanceUID !== previousProps.studyInstanceUID ||
      this.props.seriesInstanceUID !== previousProps.seriesInstanceUID ||
      this.props.slide !== previousProps.slide ||
      this.props.clients !== previousProps.clients
    ) {
      if (this.volumeViewportRef.current != null) {
        this.volumeViewportRef.current.innerHTML = "";
      }
      this.volumeViewer.cleanup();
      if (this.labelViewer != null) {
        if (this.labelViewportRef.current != null) {
          this.labelViewportRef.current.innerHTML = "";
        }
        this.labelViewer.cleanup();
      }
      const { volumeViewer, labelViewer } = _constructViewers({
        clients: this.props.clients,
        slide: this.props.slide,
        preload: this.props.preload,
      });

      this.volumeViewer = volumeViewer;
      this.labelViewer = labelViewer;

      const activeOpticalPathIdentifiers: Set<string> = new Set();
      const visibleOpticalPathIdentifiers: Set<string> = new Set();
      this.volumeViewer.getAllOpticalPaths().forEach((opticalPath) => {
        const identifier = opticalPath.identifier;
        if (this.volumeViewer.isOpticalPathVisible(identifier)) {
          visibleOpticalPathIdentifiers.add(identifier);
        }
        if (this.volumeViewer.isOpticalPathActive(identifier)) {
          activeOpticalPathIdentifiers.add(identifier);
        }
      });

      const [offset, size] = this.volumeViewer.boundingBox;

      this.setState({
        visibleRoiUIDs: new Set(),
        visibleSegmentUIDs: new Set(),
        visibleMappingUIDs: new Set(),
        visibleAnnotationGroupUIDs: new Set(),
        visibleOpticalPathIdentifiers,
        activeOpticalPathIdentifiers,
        presentationStates: [],
        loadingFrames: new Set(),
        validXCoordinateRange: [offset[0], offset[0] + size[0]],
        validYCoordinateRange: [offset[1], offset[1] + size[1]],
      });
      this.populateViewports();
    }
  }

  /**
   * Retrieve Presentation State instances that reference the any images of
   * the currently selected series.
   */
  loadPresentationStates = (): void => {
    console.info("search for Presentation State instances");
    const client =
      this.props.clients[StorageClasses.ADVANCED_BLENDING_PRESENTATION_STATE];
    client
      .searchForInstances({
        studyInstanceUID: this.props.studyInstanceUID,
        queryParams: {
          Modality: "PR",
        },
      })
      .then((matchedInstances): void => {
        if (matchedInstances == null) {
          matchedInstances = [];
        }
        matchedInstances.forEach((rawInstance, index) => {
          const { dataset } = dmv.metadata.formatMetadata(rawInstance);
          const instance = dataset as dmv.metadata.Instance;
          console.info(`retrieve PR instance "${instance.SOPInstanceUID}"`);
          client
            .retrieveInstance({
              studyInstanceUID: this.props.studyInstanceUID,
              seriesInstanceUID: instance.SeriesInstanceUID,
              sopInstanceUID: instance.SOPInstanceUID,
            })
            .then((retrievedInstance): void => {
              const data = dcmjs.data.DicomMessage.readFile(retrievedInstance);
              const { dataset } = dmv.metadata.formatMetadata(data.dict);
              if (this.props.slide.areVolumeImagesMonochrome) {
                const presentationState =
                  dataset as unknown as dmv.metadata.AdvancedBlendingPresentationState;
                let doesMatch = false;
                presentationState.AdvancedBlendingSequence.forEach(
                  (blendingItem) => {
                    doesMatch = this.props.slide.seriesInstanceUIDs.includes(
                      blendingItem.SeriesInstanceUID
                    );
                  }
                );
                if (doesMatch) {
                  console.info(
                    "include Advanced Blending Presentation State instance " +
                      `"${presentationState.SOPInstanceUID}"`
                  );
                  if (
                    index === 0 &&
                    this.props.selectedPresentationStateUID == null
                  ) {
                    this.setPresentationState(presentationState);
                  } else {
                    if (
                      presentationState.SOPInstanceUID ===
                      this.props.selectedPresentationStateUID
                    ) {
                      this.setPresentationState(presentationState);
                    }
                  }
                  this.setState((state) => {
                    const mapping: {
                      [
                        sopInstanceUID: string
                      ]: dmv.metadata.AdvancedBlendingPresentationState;
                    } = {};
                    state.presentationStates.forEach((instance) => {
                      mapping[instance.SOPInstanceUID] = instance;
                    });
                    mapping[presentationState.SOPInstanceUID] =
                      presentationState;
                    return { presentationStates: Object.values(mapping) };
                  });
                }
              } else {
                console.info(
                  `ignore presentation state "${instance.SOPInstanceUID}", ` +
                    "application of presentation states for color images " +
                    "has not (yet) been implemented"
                );
              }
            })
            .catch((error) => {
              // eslint-disable-next-line @typescript-eslint/no-floating-promises
              NotificationMiddleware.onError(
                NotificationMiddlewareContext.SLIM,
                new CustomError(
                  errorTypes.VISUALIZATION,
                  "Presentation State could not be loaded"
                )
              );
              console.error(
                "failed to load presentation state " +
                  `of SOP instance "${instance.SOPInstanceUID}" ` +
                  `of series "${instance.SeriesInstanceUID}" ` +
                  `of study "${this.props.studyInstanceUID}": `,
                error
              );
            });
        });
      })
      .catch(() => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        NotificationMiddleware.onError(
          NotificationMiddlewareContext.SLIM,
          new CustomError(
            errorTypes.VISUALIZATION,
            "Presentation State could not be loaded"
          )
        );
      });
  };

  /**
   * Set presentation state as specified by a DICOM Presentation State instance.
   */
  setPresentationState = (
    presentationState: dmv.metadata.AdvancedBlendingPresentationState
  ): void => {
    const opticalPaths = this.volumeViewer.getAllOpticalPaths();
    console.info(
      `apply Presentation State instance "${presentationState.SOPInstanceUID}"`
    );
    const opticalPathStyles: {
      [opticalPathIdentifier: string]: {
        opacity: number;
        paletteColorLookupTable?: dmv.color.PaletteColorLookupTable;
        limitValues?: number[];
      } | null;
    } = {};
    opticalPaths.forEach((opticalPath) => {
      // First, deactivate and hide all optical paths and reset style
      const identifier = opticalPath.identifier;
      this.volumeViewer.hideOpticalPath(identifier);
      this.volumeViewer.deactivateOpticalPath(identifier);
      const style = this.volumeViewer.getOpticalPathDefaultStyle(identifier);
      this.volumeViewer.setOpticalPathStyle(identifier, style);

      presentationState.AdvancedBlendingSequence.forEach((blendingItem) => {
        /**
         * Referenced Instance Sequence should be used instead of Referenced
         * Image Sequence, but that's easy to mix up and we have encountered
         * implementations that get it wrong.
         */
        let refInstanceItems = blendingItem.ReferencedInstanceSequence;
        if (refInstanceItems === undefined) {
          refInstanceItems = blendingItem.ReferencedImageSequence;
        }
        if (refInstanceItems === undefined) {
          return;
        }
        refInstanceItems.forEach((imageItem) => {
          const isReferenced = opticalPath.sopInstanceUIDs.includes(
            imageItem.ReferencedSOPInstanceUID
          ) as boolean;
          if (isReferenced) {
            let paletteColorLUT;
            if (blendingItem.PaletteColorLookupTableSequence != null) {
              const cpLUTItem = blendingItem.PaletteColorLookupTableSequence[0];
              paletteColorLUT = new dmv.color.PaletteColorLookupTable({
                uid:
                  cpLUTItem.PaletteColorLookupTableUID != null
                    ? cpLUTItem.PaletteColorLookupTableUID
                    : "",
                redDescriptor: cpLUTItem.RedPaletteColorLookupTableDescriptor,
                greenDescriptor:
                  cpLUTItem.GreenPaletteColorLookupTableDescriptor,
                blueDescriptor: cpLUTItem.BluePaletteColorLookupTableDescriptor,
                redData:
                  cpLUTItem.RedPaletteColorLookupTableData != null
                    ? new Uint16Array(cpLUTItem.RedPaletteColorLookupTableData)
                    : undefined,
                greenData:
                  cpLUTItem.GreenPaletteColorLookupTableData != null
                    ? new Uint16Array(
                        cpLUTItem.GreenPaletteColorLookupTableData
                      )
                    : undefined,
                blueData:
                  cpLUTItem.BluePaletteColorLookupTableData != null
                    ? new Uint16Array(cpLUTItem.BluePaletteColorLookupTableData)
                    : undefined,
                redSegmentedData:
                  cpLUTItem.SegmentedRedPaletteColorLookupTableData != null
                    ? new Uint16Array(
                        cpLUTItem.SegmentedRedPaletteColorLookupTableData
                      )
                    : undefined,
                greenSegmentedData:
                  cpLUTItem.SegmentedGreenPaletteColorLookupTableData != null
                    ? new Uint16Array(
                        cpLUTItem.SegmentedGreenPaletteColorLookupTableData
                      )
                    : undefined,
                blueSegmentedData:
                  cpLUTItem.SegmentedBluePaletteColorLookupTableData != null
                    ? new Uint16Array(
                        cpLUTItem.SegmentedBluePaletteColorLookupTableData
                      )
                    : undefined,
              });
            }

            let limitValues;
            if (blendingItem.SoftcopyVOILUTSequence != null) {
              const voiLUTItem = blendingItem.SoftcopyVOILUTSequence[0];
              const windowCenter = voiLUTItem.WindowCenter;
              const windowWidth = voiLUTItem.WindowWidth;
              limitValues = [
                windowCenter - windowWidth * 0.5,
                windowCenter + windowWidth * 0.5,
              ];
            }

            opticalPathStyles[identifier] = {
              opacity: 1,
              paletteColorLookupTable: paletteColorLUT,
              limitValues: limitValues,
            };
          }
        });
      });
    });

    const selectedOpticalPathIdentifiers: Set<string> = new Set();
    Object.keys(opticalPathStyles).forEach((identifier) => {
      const styleOptions = opticalPathStyles[identifier];
      if (styleOptions != null) {
        this.volumeViewer.setOpticalPathStyle(identifier, styleOptions);
        this.volumeViewer.activateOpticalPath(identifier);
        this.volumeViewer.showOpticalPath(identifier);
        selectedOpticalPathIdentifiers.add(identifier);
      } else {
        this.volumeViewer.hideOpticalPath(identifier);
        this.volumeViewer.deactivateOpticalPath(identifier);
      }
    });
    const searchParams = new URLSearchParams(this.props.location.search);
    searchParams.set("state", presentationState.SOPInstanceUID);
    this.props.navigate(
      {
        pathname: this.props.location.pathname,
        search: searchParams.toString(),
      },
      { replace: true }
    );
    this.setState((state) => ({
      activeOpticalPathIdentifiers: selectedOpticalPathIdentifiers,
      visibleOpticalPathIdentifiers: selectedOpticalPathIdentifiers,
      selectedPresentationStateUID: presentationState.SOPInstanceUID,
    }));
  };

  getRoiStyle = (key?: string): dmv.viewer.ROIStyleOptions => {
    if (key == null) {
      return this.defaultRoiStyle;
    }
    if (this.roiStyles[key] !== undefined) {
      return this.roiStyles[key];
    }
    return this.defaultRoiStyle;
  };

  /**
   * Retrieve Structured Report instances that contain regions of interests
   * with 3D spatial coordinates defined in the same frame of reference as the
   * currently selected series and add them to the VOLUME image viewer.
   */
  addAnnotations = (): void => {
    console.info("search for Comprehensive 3D SR instances");
    const client = this.props.clients[StorageClasses.COMPREHENSIVE_3D_SR];
    client
      .searchForInstances({
        studyInstanceUID: this.props.studyInstanceUID,
        queryParams: {
          Modality: "SR",
        },
      })
      .then((matchedInstances): void => {
        if (matchedInstances == null) {
          matchedInstances = [];
        }
        matchedInstances.forEach((i) => {
          const { dataset } = dmv.metadata.formatMetadata(i);
          const instance = dataset as dmv.metadata.Instance;
          if (instance.SOPClassUID === StorageClasses.COMPREHENSIVE_3D_SR) {
            console.info(`retrieve SR instance "${instance.SOPInstanceUID}"`);
            client
              .retrieveInstance({
                studyInstanceUID: this.props.studyInstanceUID,
                seriesInstanceUID: instance.SeriesInstanceUID,
                sopInstanceUID: instance.SOPInstanceUID,
              })
              .then((retrievedInstance): void => {
                const data =
                  dcmjs.data.DicomMessage.readFile(retrievedInstance);
                const { dataset } = dmv.metadata.formatMetadata(data.dict);
                const report =
                  dataset as unknown as dmv.metadata.Comprehensive3DSR;
                /*
                 * Perform a couple of checks to ensure the document content of the
                 * report fullfils the requirements of the application.
                 */
                if (!_implementsTID1500(report)) {
                  console.debug(
                    `ignore SR document "${report.SOPInstanceUID}" ` +
                      "because it is not structured according to template " +
                      'TID 1500 "MeasurementReport"'
                  );
                  return;
                }
                if (!_describesSpecimenSubject(report)) {
                  console.debug(
                    `ignore SR document "${report.SOPInstanceUID}" ` +
                      "because it does not describe a specimen subject"
                  );
                  return;
                }
                if (!_containsROIAnnotations(report)) {
                  console.debug(
                    `ignore SR document "${report.SOPInstanceUID}" ` +
                      "because it does not contain any suitable ROI annotations"
                  );
                  return;
                }

                const content = new MeasurementReport(report);
                content.ROIs.forEach((roi) => {
                  console.info(`add ROI "${roi.uid}"`);
                  const scoord3d = roi.scoord3d;
                  const image = this.props.slide.volumeImages[0];
                  if (
                    scoord3d.frameOfReferenceUID === image.FrameOfReferenceUID
                  ) {
                    /*
                     * ROIs may get assigned new UIDs upon re-rendering of the
                     * page and we need to ensure that we don't add them twice.
                     * The same ROI may be stored in multiple SR documents and
                     * we don't want them to show up twice.
                     * TODO: We should probably either "merge" measurements and
                     * quantitative evaluations or pick the ROI from the "best"
                     * available report (COMPLETE and VERIFIED).
                     */
                    const doesROIExist = this.volumeViewer
                      .getAllROIs()
                      .some((otherROI: dmv.roi.ROI): boolean => {
                        return _areROIsEqual(otherROI, roi);
                      });
                    if (!doesROIExist) {
                      try {
                        // Add ROI without style such that it won't be visible.
                        this.volumeViewer.addROI(roi, {});
                      } catch {
                        console.error(`could not add ROI "${roi.uid}"`);
                      }
                    } else {
                      console.debug(`skip already existing ROI "${roi.uid}"`);
                    }
                  } else {
                    console.debug(
                      `skip ROI "${roi.uid}" ` +
                        `of SR document "${report.SOPInstanceUID}"` +
                        "because it is defined in another frame of reference"
                    );
                  }
                });
              })
              .catch((error) => {
                // eslint-disable-next-line @typescript-eslint/no-floating-promises
                NotificationMiddleware.onError(
                  NotificationMiddlewareContext.SLIM,
                  new CustomError(
                    errorTypes.VISUALIZATION,
                    "Annotations could not be loaded"
                  )
                );
                console.error(
                  "failed to load ROIs " +
                    `of SOP instance "${instance.SOPInstanceUID}" ` +
                    `of series "${instance.SeriesInstanceUID}" ` +
                    `of study "${this.props.studyInstanceUID}": `,
                  error
                );
              });
            /*
             * React is not aware of the fact that ROIs have been added via the
             * viewer (the viewport is a ref object) and won't show the
             * annotations in the user interface unless an update is forced.
             */
            this.forceUpdate();
          }
        });
      })
      .catch(() => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        NotificationMiddleware.onError(
          NotificationMiddlewareContext.SLIM,
          new CustomError(
            errorTypes.VISUALIZATION,
            "Annotations could not be loaded"
          )
        );
      });
  };

  /**
   * Retrieve Microscopy Bulk Simple Annotations instances that contain
   * annotation groups defined in the same frame of reference as the currently
   * selected series and add them to the VOLUME image viewer.
   */
  addAnnotationGroups = (): void => {
    console.info("search for Microscopy Bulk Simple Annotations instances");
    const client =
      this.props.clients[StorageClasses.MICROSCOPY_BULK_SIMPLE_ANNOTATION];
    client
      .searchForSeries({
        studyInstanceUID: this.props.studyInstanceUID,
        queryParams: {
          Modality: "ANN",
        },
      })
      .then((matchedSeries): void => {
        if (matchedSeries == null) {
          matchedSeries = [];
        }
        matchedSeries.forEach((s) => {
          const { dataset } = dmv.metadata.formatMetadata(s);
          const series = dataset as dmv.metadata.Series;
          client
            .retrieveSeriesMetadata({
              studyInstanceUID: this.props.studyInstanceUID,
              seriesInstanceUID: series.SeriesInstanceUID,
            })
            .then((retrievedMetadata): void => {
              let annotations: dmv.metadata.MicroscopyBulkSimpleAnnotations[];
              annotations = retrievedMetadata.map((metadata) => {
                return new dmv.metadata.MicroscopyBulkSimpleAnnotations({
                  metadata,
                });
              });
              annotations = annotations.filter((ann) => {
                const refImage = this.props.slide.volumeImages[0];
                return (
                  ann.FrameOfReferenceUID === refImage.FrameOfReferenceUID &&
                  ann.ContainerIdentifier === refImage.ContainerIdentifier
                );
              });
              annotations.forEach((ann) => {
                try {
                  this.volumeViewer.addAnnotationGroups(ann);
                } catch (error: any) {
                  // eslint-disable-next-line @typescript-eslint/no-floating-promises
                  NotificationMiddleware.onError(
                    NotificationMiddlewareContext.SLIM,
                    new CustomError(
                      errorTypes.VISUALIZATION,
                      "Microscopy Bulk Simple Annotations cannot be displayed."
                    )
                  );
                  // eslint-disable-next-line @typescript-eslint/no-floating-promises
                  console.error("failed to add annotation groups: ", error);
                }
                ann.AnnotationGroupSequence.forEach((item) => {
                  const annotationGroupUID = item.AnnotationGroupUID;
                  const finding = item.AnnotationPropertyTypeCodeSequence[0];
                  const key = _buildKey(finding);
                  const style = this.roiStyles[key];
                  // eslint-disable-next-line @typescript-eslint/prefer-optional-chain
                  if (style != null && style.fill != null) {
                    this.volumeViewer.setAnnotationGroupStyle(
                      annotationGroupUID,
                      { color: style.fill.color }
                    );
                  }
                });
              });
              /*
               * React is not aware of the fact that annotation groups have been
               * added via the viewer (the underlying HTML viewport element is a
               * ref object) and won't show the annotation groups in the user
               * interface unless an update is forced.
               */
              this.forceUpdate();
            })
            .catch(() => {
              // eslint-disable-next-line @typescript-eslint/no-floating-promises
              NotificationMiddleware.onError(
                NotificationMiddlewareContext.SLIM,
                new CustomError(
                  errorTypes.VISUALIZATION,
                  "Retrieval of metadata of Microscopy Bulk Simple Annotations " +
                    "instances failed."
                )
              );
            });
        });
      })
      .catch(() => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        NotificationMiddleware.onError(
          NotificationMiddlewareContext.SLIM,
          new CustomError(
            errorTypes.VISUALIZATION,
            "Search for Microscopy Bulk Simple Annotations instances failed."
          )
        );
      });
  };

  /**
   * Retrieve Segmentation instances that contain segments defined in the same
   * frame of reference as the currently selected series and add them to the
   * VOLUME image viewer.
   */
  addSegmentations = (): void => {
    console.info("search for Segmentation instances");
    const client = this.props.clients[StorageClasses.SEGMENTATION];
    client
      .searchForSeries({
        studyInstanceUID: this.props.studyInstanceUID,
        queryParams: {
          Modality: "SEG",
        },
      })
      .then((matchedSeries): void => {
        if (matchedSeries == null) {
          matchedSeries = [];
        }
        matchedSeries.forEach((s, i) => {
          const { dataset } = dmv.metadata.formatMetadata(s);
          const series = dataset as dmv.metadata.Series;
          client
            .retrieveSeriesMetadata({
              studyInstanceUID: this.props.studyInstanceUID,
              seriesInstanceUID: series.SeriesInstanceUID,
            })
            .then((retrievedMetadata): void => {
              const segmentations: dmv.metadata.Segmentation[] = [];
              retrievedMetadata.forEach((metadata) => {
                const seg = new dmv.metadata.Segmentation({ metadata });
                const refImage = this.props.slide.volumeImages[0];
                if (
                  seg.FrameOfReferenceUID === refImage.FrameOfReferenceUID &&
                  seg.ContainerIdentifier === refImage.ContainerIdentifier
                ) {
                  segmentations.push(seg);
                }
              });
              if (segmentations.length > 0) {
                try {
                  this.volumeViewer.addSegments(segmentations);
                } catch (error: any) {
                  // eslint-disable-next-line @typescript-eslint/no-floating-promises
                  NotificationMiddleware.onError(
                    NotificationMiddlewareContext.SLIM,
                    new CustomError(
                      errorTypes.VISUALIZATION,
                      "Segmentations cannot be displayed"
                    )
                  );
                  console.error("failed to add segments: ", error);
                }
                /*
                 * React is not aware of the fact that segments have been added via
                 * the viewer (the underlying HTML viewport element is a ref object)
                 * and won't show the segments in the user interface unless an update
                 * is forced.
                 */
                this.forceUpdate();
              }
            })
            .catch(() => {
              // eslint-disable-next-line @typescript-eslint/no-floating-promises
              NotificationMiddleware.onError(
                NotificationMiddlewareContext.SLIM,
                new CustomError(
                  errorTypes.VISUALIZATION,
                  "Retrieval of metadata of Segmentation instances failed."
                )
              );
            });
        });
      })
      .catch(() => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        NotificationMiddleware.onError(
          NotificationMiddlewareContext.SLIM,
          new CustomError(
            errorTypes.VISUALIZATION,
            "Search for Segmentation instances failed."
          )
        );
      });
  };

  /**
   * Retrieve Parametric Map instances that contain mappings defined in the same
   * frame of reference as the currently selected series and add them to the
   * VOLUME image viewer.
   */
  addParametricMaps = (): void => {
    console.info("search for Parametric Map instances");
    const client = this.props.clients[StorageClasses.PARAMETRIC_MAP];
    client
      .searchForSeries({
        studyInstanceUID: this.props.studyInstanceUID,
        queryParams: {
          Modality: "OT",
        },
      })
      .then((matchedSeries): void => {
        if (matchedSeries == null) {
          matchedSeries = [];
        }
        matchedSeries.forEach((s) => {
          const { dataset } = dmv.metadata.formatMetadata(s);
          const series = dataset as dmv.metadata.Series;
          client
            .retrieveSeriesMetadata({
              studyInstanceUID: this.props.studyInstanceUID,
              seriesInstanceUID: series.SeriesInstanceUID,
            })
            .then((retrievedMetadata): void => {
              const parametricMaps: dmv.metadata.ParametricMap[] = [];
              retrievedMetadata.forEach((metadata) => {
                const pm = new dmv.metadata.ParametricMap({ metadata });
                const refImage = this.props.slide.volumeImages[0];
                if (
                  pm.FrameOfReferenceUID === refImage.FrameOfReferenceUID &&
                  pm.ContainerIdentifier === refImage.ContainerIdentifier
                ) {
                  parametricMaps.push(pm);
                } else {
                  console.warn(
                    `skip Parametric Map instance "${pm.SOPInstanceUID}"`
                  );
                }
              });
              if (parametricMaps.length > 0) {
                try {
                  this.volumeViewer.addParameterMappings(parametricMaps);
                } catch (error: any) {
                  // eslint-disable-next-line @typescript-eslint/no-floating-promises
                  NotificationMiddleware.onError(
                    NotificationMiddlewareContext.SLIM,
                    new CustomError(
                      errorTypes.VISUALIZATION,
                      "Parametric Map cannot be displayed"
                    )
                  );
                  console.error("failed to add mappings: ", error);
                }
                /*
                 * React is not aware of the fact that mappings have been added via
                 * the viewer (the underlying HTML viewport element is a ref object)
                 * and won't show the mappings in the user interface unless an update
                 * is forced.
                 */
                this.forceUpdate();
              }
            })
            .catch(() => {
              // eslint-disable-next-line @typescript-eslint/no-floating-promises
              NotificationMiddleware.onError(
                NotificationMiddlewareContext.SLIM,
                new CustomError(
                  errorTypes.VISUALIZATION,
                  "Retrieval of metadata of Parametric Map instances failed."
                )
              );
            });
        });
      })
      .catch(() => {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        NotificationMiddleware.onError(
          NotificationMiddlewareContext.SLIM,
          new CustomError(
            errorTypes.VISUALIZATION,
            "Search for Parametric Map instances failed."
          )
        );
      });
  };

  /**
   * Populate viewports of the VOLUME and LABEL image viewers.
   */
  populateViewports = (): void => {
    console.info("populate viewports...");
    this.setState({
      isLoading: true,
      presentationStates: [],
    });

    if (this.volumeViewportRef.current != null) {
      this.volumeViewer.render({ container: this.volumeViewportRef.current });
    }
    if (this.labelViewportRef.current != null && this.labelViewer != null) {
      this.labelViewer.render({ container: this.labelViewportRef.current });
    }

    // State update will also ensure that the component is re-rendered.
    this.setState({ isLoading: false });

    this.setDefaultPresentationState();
    this.loadPresentationStates();

    this.addAnnotations();
    this.addAnnotationGroups();
    this.addSegmentations();
    this.addParametricMaps();
  };

  onRoiModified = (event: CustomEventInit): void => {
    // Update state to trigger rendering
    this.setState((state) => ({
      visibleRoiUIDs: new Set(state.visibleRoiUIDs),
    }));
  };

  onWindowResize = (event: Event): void => {
    console.info("resize viewports");
    this.volumeViewer.resize();
    if (this.labelViewer != null) {
      this.labelViewer.resize();
    }
  };

  onRoiDrawn = (event: CustomEventInit): void => {
    const roi = event.detail.payload as dmv.roi.ROI;
    const selectedFinding = this.state.selectedFinding;
    const selectedEvaluations = this.state.selectedEvaluations;
    if (roi !== undefined && selectedFinding !== undefined) {
      console.debug(`add ROI "${roi.uid}"`);
      const findingItem = new dcmjs.sr.valueTypes.CodeContentItem({
        name: new dcmjs.sr.coding.CodedConcept({
          value: "121071",
          meaning: "Finding",
          schemeDesignator: "DCM",
        }),
        value: selectedFinding,
        relationshipType: "CONTAINS",
      });
      roi.addEvaluation(findingItem);
      selectedEvaluations.forEach((evaluation: Evaluation) => {
        const item = new dcmjs.sr.valueTypes.CodeContentItem({
          name: evaluation.name,
          value: evaluation.value,
          relationshipType: "CONTAINS",
        });
        roi.addEvaluation(item);
      });
      const key = _buildKey(selectedFinding);
      const style = this.getRoiStyle(key);
      this.volumeViewer.addROI(roi, style);
      this.setState((state) => {
        const visibleRoiUIDs = state.visibleRoiUIDs;
        visibleRoiUIDs.add(roi.uid);
        return { visibleRoiUIDs };
      });
    } else {
      console.debug(`could not add ROI "${roi.uid}"`);
    }
  };

  onRoiSelected = (event: CustomEventInit): void => {
    const selectedRoi = event.detail.payload as dmv.roi.ROI;
    if (selectedRoi != null) {
      console.debug(`selected ROI "${selectedRoi.uid}"`);
      this.volumeViewer.setROIStyle(selectedRoi.uid, this.selectedRoiStyle);
      const key = _getRoiKey(selectedRoi);
      this.volumeViewer.getAllROIs().forEach((roi) => {
        if (roi.uid !== selectedRoi.uid) {
          this.volumeViewer.setROIStyle(roi.uid, this.getRoiStyle(key));
        }
      });
      this.setState({
        selectedRoiUIDs: new Set([selectedRoi.uid]),
        selectedRoi: selectedRoi,
        isSelectedRoiModalVisible: true,
      });
    } else {
      this.setState({
        selectedRoiUIDs: new Set(),
        selectedRoi: undefined,
        isSelectedRoiModalVisible: false,
      });
    }
  };

  handleRoiSelectionCancellation(): void {
    this.setState({
      isSelectedRoiModalVisible: false,
      selectedRoiUIDs: new Set(),
    });
  }

  onLoadingStarted = (event: CustomEventInit): void => {
    this.setState({ isLoading: true });
  };

  onLoadingEnded = (event: CustomEventInit): void => {
    this.setState({ isLoading: false });
  };

  onFrameLoadingStarted = (event: CustomEventInit): void => {
    const frameInfo: {
      studyInstanceUID: string;
      seriesInstanceUID: string;
      sopInstanceUID: string;
      sopClassUID: string;
      frameNumber: string;
      channelIdentifier: string;
    } = event.detail.payload;
    const key: string = `${frameInfo.sopInstanceUID}-${frameInfo.frameNumber}`;
    this.setState((state) => {
      state.loadingFrames.add(key);
      return state;
    });
  };

  onFrameLoadingEnded = (event: CustomEventInit): void => {
    const frameInfo: {
      studyInstanceUID: string;
      seriesInstanceUID: string;
      sopInstanceUID: string;
      sopClassUID: string;
      frameNumber: string;
      channelIdentifier: string;
      pixelArray: Uint8Array | Uint16Array | Float32Array | null;
    } = event.detail.payload;
    const key = `${frameInfo.sopInstanceUID}-${frameInfo.frameNumber}`;
    this.setState((state) => {
      state.loadingFrames.delete(key);
      let isLoading: boolean = false;
      if (state.loadingFrames.size > 0) {
        isLoading = true;
      }
      return {
        isLoading,
        loadingFrames: state.loadingFrames,
      };
    });
    if (
      frameInfo.sopClassUID ===
        StorageClasses.VL_WHOLE_SLIDE_MICROSCOPY_IMAGE &&
      this.props.slide.areVolumeImagesMonochrome
    ) {
      const opticalPathIdentifier = frameInfo.channelIdentifier;
      if (
        !(opticalPathIdentifier in this.state.pixelDataStatistics) &&
        frameInfo.pixelArray != null
      ) {
        /*
         * There are limits on the number of arguments Math.min and Math.max
         * functions can accept. Therefore, we compute values in smaller chunks.
         */
        const size = 2 ** 16;
        const chunks = Math.ceil(frameInfo.pixelArray.length / size);
        let offset = 0;
        const minValues: number[] = [];
        const maxValues: number[] = [];
        for (let i = 0; i < chunks; i++) {
          offset = i * size;
          const pixels = frameInfo.pixelArray.slice(offset, offset + size);
          minValues.push(Math.min(...pixels));
          maxValues.push(Math.max(...pixels));
        }
        const min = Math.min(...minValues);
        const max = Math.max(...maxValues);
        this.setState((state) => {
          const stats = state.pixelDataStatistics;
          if (stats[opticalPathIdentifier] != null) {
            stats[opticalPathIdentifier] = {
              min: Math.min(stats[opticalPathIdentifier].min, min),
              max: Math.max(stats[opticalPathIdentifier].max, max),
              numFramesSampled:
                stats[opticalPathIdentifier].numFramesSampled + 1,
            };
          } else {
            stats[opticalPathIdentifier] = {
              min: min,
              max: max,
              numFramesSampled: 1,
            };
          }
          if (state.selectedPresentationStateUID == null) {
            const style = {
              ...this.volumeViewer.getOpticalPathStyle(opticalPathIdentifier),
            };
            style.limitValues = [
              stats[opticalPathIdentifier].min,
              stats[opticalPathIdentifier].max,
            ];
            this.volumeViewer.setOpticalPathStyle(opticalPathIdentifier, style);
          }
          return state;
        });
      }
    }
  };

  onRoiRemoved = (event: CustomEventInit): void => {
    const roi = event.detail.payload as dmv.roi.ROI;
    console.debug(`removed ROI "${roi.uid}"`);
  };

  componentCleanup(): void {
    document.body.removeEventListener(
      "dicommicroscopyviewer_roi_drawn",
      this.onRoiDrawn
    );
    document.body.removeEventListener(
      "dicommicroscopyviewer_roi_selected",
      this.onRoiSelected
    );
    document.body.removeEventListener(
      "dicommicroscopyviewer_roi_removed",
      this.onRoiRemoved
    );
    document.body.removeEventListener(
      "dicommicroscopyviewer_roi_modified",
      this.onRoiModified
    );
    document.body.removeEventListener(
      "dicommicroscopyviewer_loading_started",
      this.onLoadingStarted
    );
    document.body.removeEventListener(
      "dicommicroscopyviewer_loading_ended",
      this.onLoadingEnded
    );
    document.body.removeEventListener(
      "dicommicroscopyviewer_frame_loading_started",
      this.onFrameLoadingStarted
    );
    document.body.removeEventListener(
      "dicommicroscopyviewer_frame_loading_ended",
      this.onFrameLoadingEnded
    );
    document.body.removeEventListener("keyup", this.onKeyUp);
    window.removeEventListener("resize", this.onWindowResize);

    this.volumeViewer.cleanup();
    if (this.labelViewer != null) {
      this.labelViewer.cleanup();
    }
    /*
     * FIXME: React appears to not clean the content of referenced
     * HTMLDivElement objects when the page is reloaded. As a consequence,
     * optical paths and other display items cannot be toggled or updated after
     * a manual page reload. I have tried using ref callbacks and passing the
     * ref objects from the parent component via the props. Both didn't work
     * either.
     */
  }

  onKeyUp = (event: KeyboardEvent): void => {
    if (event.key === "Escape") {
      if (this.state.isRoiDrawingActive) {
        console.info("deactivate drawing of ROIs");
        this.volumeViewer.deactivateDrawInteraction();
        this.volumeViewer.activateSelectInteraction({});
      } else if (this.state.isRoiModificationActive) {
        console.info("deactivate modification of ROIs");
        this.volumeViewer.deactivateModifyInteraction();
        this.volumeViewer.activateSelectInteraction({});
      } else if (this.state.isRoiTranslationActive) {
        console.info("deactivate modification of ROIs");
        this.volumeViewer.deactivateTranslateInteraction();
        this.volumeViewer.activateSelectInteraction({});
      }
      this.setState({
        isAnnotationModalVisible: false,
        isSelectedRoiModalVisible: false,
        isRoiTranslationActive: false,
        isRoiDrawingActive: false,
        isRoiModificationActive: false,
        isGoToModalVisible: false,
      });
    } else if (event.altKey) {
      if (event.code === "KeyD") {
        this.handleRoiDrawing();
      } else if (event.code === "KeyM") {
        this.handleRoiModification();
      } else if (event.code === "KeyT") {
        this.handleRoiTranslation();
      } else if (event.code === "KeyR") {
        this.handleRoiRemoval();
      } else if (event.code === "KeyV") {
        this.handleRoiVisibilityChange();
      } else if (event.code === "KeyS") {
        this.handleReportGeneration();
      } else if (event.code === "KeyG") {
        this.handleGoTo();
      }
    }
  };

  componentWillUnmount(): void {
    window.removeEventListener("beforeunload", this.componentCleanup);
  }

  componentSetup(): void {
    document.body.addEventListener(
      "dicommicroscopyviewer_roi_drawn",
      this.onRoiDrawn
    );
    document.body.addEventListener(
      "dicommicroscopyviewer_roi_selected",
      this.onRoiSelected
    );
    document.body.addEventListener(
      "dicommicroscopyviewer_roi_removed",
      this.onRoiRemoved
    );
    document.body.addEventListener(
      "dicommicroscopyviewer_roi_modified",
      this.onRoiModified
    );
    document.body.addEventListener(
      "dicommicroscopyviewer_loading_started",
      this.onLoadingStarted
    );
    document.body.addEventListener(
      "dicommicroscopyviewer_loading_ended",
      this.onLoadingEnded
    );
    document.body.addEventListener(
      "dicommicroscopyviewer_frame_loading_started",
      this.onFrameLoadingStarted
    );
    document.body.addEventListener(
      "dicommicroscopyviewer_frame_loading_ended",
      this.onFrameLoadingEnded
    );
    document.body.addEventListener("keyup", this.onKeyUp);
    window.addEventListener("resize", this.onWindowResize);
  }

  componentDidMount(): void {
    window.addEventListener("beforeunload", this.componentCleanup);
    this.componentSetup();
    this.populateViewports();

    if (!this.props.slide.areVolumeImagesMonochrome) {
      let hasICCProfile = false;
      const image = this.props.slide.volumeImages[0];
      const metadataItem = image.OpticalPathSequence[0];
      if (metadataItem.ICCProfile == null) {
        if ("OpticalPathSequence" in image.bulkdataReferences) {
          // @ts-expect-error
          const bulkdataItem = image.bulkdataReferences.OpticalPathSequence[0];
          if ("ICCProfile" in bulkdataItem) {
            hasICCProfile = true;
          }
        }
      } else {
        hasICCProfile = true;
      }
      if (!hasICCProfile) {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        message.warning("No ICC Profile was found for color images");
      }
    }
  }

  /**
   * Handler that gets called when a finding has been selected for annotation.
   *
   * @param value - Code value of the coded finding that got selected
   * @param option - Option that got selected
   */
  handleAnnotationFindingSelection(value: string, option: any): void {
    this.findingOptions.forEach((finding) => {
      if (finding.CodeValue === value) {
        console.info(`selected finding "${finding.CodeMeaning}"`);
        this.setState({
          selectedFinding: finding,
          selectedEvaluations: [],
        });
      }
    });
  }

  /**
   * Handler that gets called when a geometry type has been selected for
   * annotation.
   *
   * @param value - Code value of the coded finding that got selected
   * @param option - Option that got selected
   */
  handleAnnotationGeometryTypeSelection(value: string, option: any): void {
    this.setState({ selectedGeometryType: value });
  }

  /**
   * Handler that gets called when measurements have been selected for
   * annotation.
   */
  handleAnnotationMeasurementActivation(event: any): void {
    const active: boolean = event.target.checked;
    if (active) {
      this.setState({ selectedMarkup: "measurement" });
    } else {
      this.setState({ selectedMarkup: undefined });
    }
  }

  /**
   * Handler that gets called when an evaluation has been selected for an
   * annotation.
   *
   * @param value - Code value of the coded evaluation that got selected
   * @param option - Option that got selected
   */
  handleAnnotationEvaluationSelection(value: string, option: any): void {
    const selectedFinding = this.state.selectedFinding;
    if (selectedFinding !== undefined) {
      const key = _buildKey(selectedFinding);
      const name = option.label;
      this.evaluationOptions[key].forEach((evaluation) => {
        if (
          evaluation.name.CodeValue === name.CodeValue &&
          evaluation.name.CodingSchemeDesignator === name.CodingSchemeDesignator
        ) {
          evaluation.values.forEach((code) => {
            if (code.CodeValue === value) {
              const filteredEvaluations = this.state.selectedEvaluations.filter(
                (item: Evaluation) => item.name !== evaluation.name
              );
              this.setState({
                selectedEvaluations: [
                  ...filteredEvaluations,
                  { name: name, value: code },
                ],
              });
            }
          });
        }
      });
    }
  }

  /**
   * Handler that gets called when an evaluation has been cleared for an
   * annotation.
   */
  handleAnnotationEvaluationClearance(): void {
    this.setState({
      selectedEvaluations: [],
    });
  }

  handleXCoordinateSelection(value: any): void {
    if (value != null) {
      const x = Number(value);
      const start = this.state.validXCoordinateRange[0];
      const end = this.state.validXCoordinateRange[1];
      if (x >= start && x <= end) {
        this.setState({
          selectedXCoordinate: x,
          isSelectedXCoordinateValid: true,
        });
        return;
      }
    }
    this.setState({
      selectedXCoordinate: undefined,
      isSelectedXCoordinateValid: false,
    });
  }

  handleYCoordinateSelection(value: any): void {
    if (value != null) {
      const y = Number(value);
      const start = this.state.validYCoordinateRange[0];
      const end = this.state.validYCoordinateRange[1];
      if (y >= start && y <= end) {
        this.setState({
          selectedYCoordinate: y,
          isSelectedYCoordinateValid: true,
        });
        return;
      }
    }
    this.setState({
      selectedYCoordinate: undefined,
      isSelectedYCoordinateValid: false,
    });
  }

  handleMagnificationSelection(value: any): void {
    if (value != null) {
      if (value > 0 && value <= 40) {
        this.setState({
          selectedMagnification: Number(value),
          isSelectedMagnificationValid: true,
        });
        return;
      }
    }
    this.setState({
      selectedMagnification: undefined,
      isSelectedMagnificationValid: false,
    });
  }

  /**
   * Handler that gets called when the selection of slide position was
   * completed.
   */
  handleSlidePositionSelection(): void {
    if (
      this.state.isSelectedXCoordinateValid &&
      this.state.isSelectedYCoordinateValid &&
      this.state.isSelectedMagnificationValid &&
      this.state.selectedXCoordinate != null &&
      this.state.selectedYCoordinate != null &&
      this.state.selectedMagnification != null
    ) {
      console.info(
        "select slide position " +
          `(${this.state.selectedXCoordinate}, ` +
          `${this.state.selectedYCoordinate}) ` +
          `at ${this.state.selectedMagnification}x magnification`
      );

      const factor = this.state.selectedMagnification;
      /**
       * On an optical microscope an objective with 1x magnification
       * corresponds to approximately 10 micrometer pixel spacing
       * (due to the ocular).
       */
      const targetPixelSpacing = 0.01 / factor;
      const diffs = [];
      for (let i = 0; i < this.volumeViewer.numLevels; i++) {
        const actualPixelSpacing = this.volumeViewer.getPixelSpacing(i)[0];
        diffs.push(Math.abs(targetPixelSpacing - actualPixelSpacing));
      }
      const level = diffs.indexOf(Math.min(...diffs));
      this.volumeViewer.navigate({
        position: [
          this.state.selectedXCoordinate,
          this.state.selectedYCoordinate,
        ],
        level: level,
      });
      const point = new dmv.scoord3d.Point({
        coordinates: [
          this.state.selectedXCoordinate,
          this.state.selectedYCoordinate,
          0,
        ],
        frameOfReferenceUID: this.volumeViewer.frameOfReferenceUID,
      });
      const roi = new dmv.roi.ROI({ scoord3d: point });
      this.volumeViewer.addROI(roi, this.defaultRoiStyle);
      this.setState((state) => {
        const visibleRoiUIDs = state.visibleRoiUIDs;
        visibleRoiUIDs.add(roi.uid);
        return {
          visibleRoiUIDs,
          isGoToModalVisible: false,
        };
      });
    }
  }

  /**
   * Handler that gets called when the selection of a slide position was
   * canceled.
   */
  handleSlidePositionSelectionCancellation(): void {
    console.log("cancel slide position selection");
    this.setState({
      isGoToModalVisible: false,
      isSelectedXCoordinateValid: false,
      isSelectedYCoordinateValid: false,
      isSelectedMagnificationValid: false,
      selectedXCoordinate: undefined,
      selectedYCoordinate: undefined,
      selectedMagnification: undefined,
    });
  }

  /**
   * Handler that gets called when annotation configuration has been completed.
   */
  handleAnnotationConfigurationCompletion(): void {
    console.debug("complete annotation configuration");
    const finding = this.state.selectedFinding;
    const geometryType = this.state.selectedGeometryType;
    const markup = this.state.selectedMarkup;
    if (geometryType !== undefined && finding !== undefined) {
      this.volumeViewer.activateDrawInteraction({ geometryType, markup });
      this.setState({
        isAnnotationModalVisible: false,
        isRoiDrawingActive: true,
      });
    } else {
      NotificationMiddleware.onError(
        NotificationMiddlewareContext.SLIM,
        new CustomError(
          errorTypes.VISUALIZATION,
          "Could not complete annotation configuration"
        )
      );
    }
  }

  /**
   * Handler that gets called when annotation configuration has been cancelled.
   */
  handleAnnotationConfigurationCancellation(): void {
    console.debug("cancel annotation configuration");
    this.setState({
      isAnnotationModalVisible: false,
      isRoiDrawingActive: false,
    });
  }

  /**
   * Handler that gets called when a report should be generated for the current
   * set of annotations.
   */
  handleReportGeneration(): void {
    console.info("save ROIs");
    const rois = this.volumeViewer.getAllROIs();
    const opticalPaths = this.volumeViewer.getAllOpticalPaths();
    const metadata = this.volumeViewer.getOpticalPathMetadata(
      opticalPaths[0].identifier
    );
    // Metadata should be sorted such that the image with the highest
    // resolution is the last item in the array.
    const refImage = metadata[metadata.length - 1];
    // We assume that there is only one specimen (tissue section) per
    // ontainer (slide). Only the tissue section is tracked with a unique
    // identifier, even if the section may be composed of different biological
    // samples.
    if (refImage.SpecimenDescriptionSequence.length > 1) {
      NotificationMiddleware.onError(
        NotificationMiddlewareContext.SLIM,
        new CustomError(
          errorTypes.VISUALIZATION,
          "More than one specimen has been described for the slide"
        )
      );
    }
    const refSpecimen = refImage.SpecimenDescriptionSequence[0];

    console.debug("create Observation Context");
    let observer;
    if (this.props.user !== undefined) {
      observer = new dcmjs.sr.templates.PersonObserverIdentifyingAttributes({
        name: this.props.user.name,
        loginName: this.props.user.email,
      });
    } else {
      console.warn("no user information available");
      observer = new dcmjs.sr.templates.PersonObserverIdentifyingAttributes({
        name: "ANONYMOUS",
      });
    }
    const observationContext = new dcmjs.sr.templates.ObservationContext({
      observerPersonContext: new dcmjs.sr.templates.ObserverContext({
        observerType: new dcmjs.sr.coding.CodedConcept({
          value: "121006",
          schemeDesignator: "DCM",
          meaning: "Person",
        }),
        observerIdentifyingAttributes: observer,
      }),
      observerDeviceContext: new dcmjs.sr.templates.ObserverContext({
        observerType: new dcmjs.sr.coding.CodedConcept({
          value: "121007",
          schemeDesignator: "DCM",
          meaning: "Device",
        }),
        observerIdentifyingAttributes:
          new dcmjs.sr.templates.DeviceObserverIdentifyingAttributes({
            uid: this.props.appInfo.uid,
            manufacturerName: "MGH Computational Pathology",
            modelName: this.props.appInfo.name,
          }),
      }),
      subjectContext: new dcmjs.sr.templates.SubjectContext({
        subjectClass: new dcmjs.sr.coding.CodedConcept({
          value: "121027",
          schemeDesignator: "DCM",
          meaning: "Specimen",
        }),
        subjectClassSpecificContext:
          new dcmjs.sr.templates.SubjectContextSpecimen({
            uid: refSpecimen.SpecimenUID,
            identifier: refSpecimen.SpecimenIdentifier,
            containerIdentifier: refImage.ContainerIdentifier,
          }),
      }),
    });

    console.debug("encode Imaging Measurements");
    const imagingMeasurements: dcmjs.sr.valueTypes.ContainerContentItem[] = [];
    for (let i = 0; i < rois.length; i++) {
      const roi = rois[i];
      if (!this.state.visibleRoiUIDs.has(roi.uid)) {
        continue;
      }
      let findingType = roi.evaluations.find(
        (item: dcmjs.sr.valueTypes.ContentItem) => {
          return item.ConceptNameCodeSequence[0].CodeValue === "121071";
        }
      );
      if (findingType === undefined) {
        NotificationMiddleware.onError(
          NotificationMiddlewareContext.SLIM,
          new CustomError(
            errorTypes.ENCODINGANDDECODING,
            `No finding type was specified for ROI "${roi.uid}"`
          )
        );
      }
      findingType = findingType as dcmjs.sr.valueTypes.CodeContentItem;
      const group =
        new dcmjs.sr.templates.PlanarROIMeasurementsAndQualitativeEvaluations({
          trackingIdentifier: new dcmjs.sr.templates.TrackingIdentifier({
            uid: roi.properties.trackingUID ?? roi.uid,
            identifier: `ROI #${i + 1}`,
          }),
          referencedRegion: new dcmjs.sr.contentItems.ImageRegion3D({
            graphicType: roi.scoord3d.graphicType,
            graphicData: roi.scoord3d.graphicData,
            frameOfReferenceUID: roi.scoord3d.frameOfReferenceUID,
          }),
          findingType: new dcmjs.sr.coding.CodedConcept({
            value: findingType.ConceptCodeSequence[0].CodeValue,
            schemeDesignator:
              findingType.ConceptCodeSequence[0].CodingSchemeDesignator,
            meaning: findingType.ConceptCodeSequence[0].CodeMeaning,
          }),
          qualitativeEvaluations: roi.evaluations.filter(
            (item: dcmjs.sr.valueTypes.ContentItem) => {
              return item.ConceptNameCodeSequence[0].CodeValue !== "121071";
            }
          ),
          measurements: roi.measurements,
        });
      const measurements = group as dcmjs.sr.valueTypes.ContainerContentItem[];
      measurements[0].ContentTemplateSequence = [
        {
          MappingResource: "DCMR",
          TemplateIdentifier: "1410",
        },
      ];
      imagingMeasurements.push(...measurements);
    }

    console.debug("create Measurement Report document content");
    const measurementReport = new dcmjs.sr.templates.MeasurementReport({
      languageOfContentItemAndDescendants:
        new dcmjs.sr.templates.LanguageOfContentItemAndDescendants({}),
      observationContext: observationContext,
      procedureReported: new dcmjs.sr.coding.CodedConcept({
        value: "112703",
        schemeDesignator: "DCM",
        meaning: "Whole Slide Imaging",
      }),
      imagingMeasurements: imagingMeasurements,
    });

    console.info("create Comprehensive 3D SR document");
    const dataset = new dcmjs.sr.documents.Comprehensive3DSR({
      content: measurementReport[0],
      evidence: [refImage],
      seriesInstanceUID: dcmjs.data.DicomMetaDictionary.uid(),
      seriesNumber: 1,
      seriesDescription: "Annotation",
      sopInstanceUID: dcmjs.data.DicomMetaDictionary.uid(),
      instanceNumber: 1,
      manufacturer: "MGH Computational Pathology",
      previousVersions: undefined, // TODO
    });

    this.setState({
      isReportModalVisible: true,
      generatedReport: dataset as dmv.metadata.Comprehensive3DSR,
    });
  }

  /**
   * Handler that gets called when a report should be verified. The current
   * list of annotations will be presented to the user together with other
   * pertinent metadata about the patient, study, and specimen.
   */
  handleReportVerification(): void {
    console.info("verfied report");

    const report = this.state.generatedReport;
    if (report !== undefined) {
      const dataset = report as unknown as dmv.metadata.Comprehensive3DSR;
      console.debug("create File Meta Information");
      const fileMetaInformationVersionArray = new Uint8Array(2);
      fileMetaInformationVersionArray[1] = 1;
      const fileMeta = {
        // FileMetaInformationVersion
        "00020001": {
          Value: [fileMetaInformationVersionArray.buffer],
          vr: "OB",
        },
        // MediaStorageSOPClassUID
        "00020002": {
          Value: [dataset.SOPClassUID],
          vr: "UI",
        },
        // MediaStorageSOPInstanceUID
        "00020003": {
          Value: [dataset.SOPInstanceUID],
          vr: "UI",
        },
        // TransferSyntaxUID
        "00020010": {
          Value: ["1.2.840.10008.1.2.1"],
          vr: "UI",
        },
        // ImplementationClassUID
        "00020012": {
          Value: [this.props.appInfo.uid],
          vr: "UI",
        },
      };

      console.info("store Comprehensive 3D SR document");
      const writer = new dcmjs.data.DicomDict(fileMeta);
      writer.dict = dcmjs.data.DicomMetaDictionary.denaturalizeDataset(dataset);
      const buffer = writer.write();
      const client = this.props.clients[StorageClasses.COMPREHENSIVE_3D_SR];
      client
        .storeInstances({ datasets: [buffer] })
        .then((response: any) => message.info("Annotations were saved."))
        .catch(() => {
          // eslint-disable-next-line @typescript-eslint/no-floating-promises
          NotificationMiddleware.onError(
            NotificationMiddlewareContext.SLIM,
            new CustomError(
              errorTypes.ENCODINGANDDECODING,
              "Annotations could not be saved"
            )
          );
        });
    }
    this.setState({
      isReportModalVisible: false,
      generatedReport: undefined,
    });
  }

  /**
   * Handler that gets called when report generation has been cancelled.
   */
  handleReportCancellation(): void {
    this.setState({
      isReportModalVisible: false,
      generatedReport: undefined,
    });
  }

  /**
   * Handler that gets called when an annotation has been selected from the
   * current list of annotations.
   */
  handleAnnotationSelection({ roiUID }: { roiUID: string }): void {
    console.log(`selected ROI ${roiUID}`);
    this.setState({ selectedRoiUIDs: new Set([roiUID]) });
    this.volumeViewer.getAllROIs().forEach((roi) => {
      let style = {};
      if (roi.uid === roiUID) {
        style = this.selectedRoiStyle;
        this.setState((state) => {
          const visibleRoiUIDs = state.visibleRoiUIDs;
          visibleRoiUIDs.add(roi.uid);
          return { visibleRoiUIDs };
        });
      } else {
        if (this.state.visibleRoiUIDs.has(roi.uid)) {
          const key = _getRoiKey(roi);
          style = this.getRoiStyle(key);
        }
      }
      this.volumeViewer.setROIStyle(roi.uid, style);
    });
  }

  /**
   * Handle toggling of annotation visibility, i.e., whether a given
   * annotation should be either displayed or hidden by the viewer.
   */
  handleAnnotationVisibilityChange({
    roiUID,
    isVisible,
  }: {
    roiUID: string;
    isVisible: boolean;
  }): void {
    if (isVisible) {
      console.info(`show ROI ${roiUID}`);
      const roi = this.volumeViewer.getROI(roiUID);
      const key = _getRoiKey(roi);
      this.volumeViewer.setROIStyle(roi.uid, this.getRoiStyle(key));
      this.setState((state) => {
        const visibleRoiUIDs = state.visibleRoiUIDs;
        visibleRoiUIDs.add(roi.uid);
        return { visibleRoiUIDs };
      });
    } else {
      console.info(`hide ROI ${roiUID}`);
      this.setState((state) => {
        const selectedRoiUIDs = state.selectedRoiUIDs;
        selectedRoiUIDs.delete(roiUID);
        const visibleRoiUIDs = state.visibleRoiUIDs;
        visibleRoiUIDs.delete(roiUID);
        return { visibleRoiUIDs, selectedRoiUIDs };
      });
      this.volumeViewer.setROIStyle(roiUID, {});
    }
  }

  /**
   * Handle toggling of annotation group visibility, i.e., whether a given
   * annotation group should be either displayed or hidden by the viewer.
   */
  handleAnnotationGroupVisibilityChange({
    annotationGroupUID,
    isVisible,
  }: {
    annotationGroupUID: string;
    isVisible: boolean;
  }): void {
    console.log(`change visibility of annotation group ${annotationGroupUID}`);
    if (isVisible) {
      console.info(`show annotation group ${annotationGroupUID}`);
      try {
        this.volumeViewer.showAnnotationGroup(annotationGroupUID);
      } catch (error) {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        NotificationMiddleware.onError(
          NotificationMiddlewareContext.SLIM,
          new CustomError(
            errorTypes.VISUALIZATION,
            "Failed to show annotation group."
          )
        );
        throw error;
      }
      this.setState((state) => {
        const visibleAnnotationGroupUIDs = new Set(
          state.visibleAnnotationGroupUIDs
        );
        visibleAnnotationGroupUIDs.add(annotationGroupUID);
        return { visibleAnnotationGroupUIDs };
      });
    } else {
      console.info(`hide annotation group ${annotationGroupUID}`);
      this.volumeViewer.hideAnnotationGroup(annotationGroupUID);
      this.setState((state) => {
        const visibleAnnotationGroupUIDs = new Set(
          state.visibleAnnotationGroupUIDs
        );
        visibleAnnotationGroupUIDs.delete(annotationGroupUID);
        return { visibleAnnotationGroupUIDs };
      });
    }
  }

  /**
   * Handle change of annotation group style.
   */
  handleAnnotationGroupStyleChange({
    annotationGroupUID,
    styleOptions,
  }: {
    annotationGroupUID: string;
    styleOptions: {
      opacity?: number;
      color?: number[];
      measurement?: dcmjs.sr.coding.CodedConcept;
    };
  }): void {
    console.log(`change style of annotation group ${annotationGroupUID}`);
    try {
      this.volumeViewer.setAnnotationGroupStyle(
        annotationGroupUID,
        styleOptions
      );
    } catch (error) {
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      NotificationMiddleware.onError(
        NotificationMiddlewareContext.SLIM,
        new CustomError(
          errorTypes.VISUALIZATION,
          "Failed to change style of annotation group."
        )
      );
      throw error;
    }
  }

  /**
   * Handle toggling of segment visibility, i.e., whether a given
   * segment should be either displayed or hidden by the viewer.
   */
  handleSegmentVisibilityChange({
    segmentUID,
    isVisible,
  }: {
    segmentUID: string;
    isVisible: boolean;
  }): void {
    console.log(`change visibility of segment ${segmentUID}`);
    if (isVisible) {
      console.info(`show segment ${segmentUID}`);
      this.volumeViewer.showSegment(segmentUID);
      this.setState((state) => {
        const visibleSegmentUIDs = new Set(state.visibleSegmentUIDs);
        visibleSegmentUIDs.add(segmentUID);
        return { visibleSegmentUIDs };
      });
    } else {
      console.info(`hide segment ${segmentUID}`);
      this.volumeViewer.hideSegment(segmentUID);
      this.setState((state) => {
        const visibleSegmentUIDs = new Set(state.visibleSegmentUIDs);
        visibleSegmentUIDs.delete(segmentUID);
        return { visibleSegmentUIDs };
      });
    }
  }

  /**
   * Handle change of segment style.
   */
  handleSegmentStyleChange({
    segmentUID,
    styleOptions,
  }: {
    segmentUID: string;
    styleOptions: {
      opacity?: number;
    };
  }): void {
    console.log(`change style of segment ${segmentUID}`);
    this.volumeViewer.setSegmentStyle(segmentUID, styleOptions);
  }

  /**
   * Handle toggling of mapping visibility, i.e., whether a given
   * mapping should be either displayed or hidden by the viewer.
   */
  handleMappingVisibilityChange({
    mappingUID,
    isVisible,
  }: {
    mappingUID: string;
    isVisible: boolean;
  }): void {
    console.log(`change visibility of mapping ${mappingUID}`);
    if (isVisible) {
      console.info(`show mapping ${mappingUID}`);
      this.volumeViewer.showParameterMapping(mappingUID);
      this.setState((state) => {
        const visibleMappingUIDs = new Set(state.visibleMappingUIDs);
        visibleMappingUIDs.add(mappingUID);
        return { visibleMappingUIDs };
      });
    } else {
      console.info(`hide mapping ${mappingUID}`);
      this.volumeViewer.hideParameterMapping(mappingUID);
      this.setState((state) => {
        const visibleMappingUIDs = new Set(state.visibleMappingUIDs);
        visibleMappingUIDs.delete(mappingUID);
        return { visibleMappingUIDs };
      });
    }
  }

  /**
   * Handle change of mapping style.
   */
  handleMappingStyleChange({
    mappingUID,
    styleOptions,
  }: {
    mappingUID: string;
    styleOptions: {
      opacity?: number;
    };
  }): void {
    console.log(`change style of mapping ${mappingUID}`);
    this.volumeViewer.setParameterMappingStyle(mappingUID, styleOptions);
  }

  /**
   * Handle toggling of optical path visibility, i.e., whether a given
   * optical path should be either displayed or hidden by the viewer.
   */
  handleOpticalPathVisibilityChange({
    opticalPathIdentifier,
    isVisible,
  }: {
    opticalPathIdentifier: string;
    isVisible: boolean;
  }): void {
    console.log(`change visibility of optical path ${opticalPathIdentifier}`);
    if (isVisible) {
      console.info(`show optical path ${opticalPathIdentifier}`);
      this.volumeViewer.showOpticalPath(opticalPathIdentifier);
      this.setState((state) => {
        const visibleOpticalPathIdentifiers = new Set(
          state.visibleOpticalPathIdentifiers
        );
        visibleOpticalPathIdentifiers.add(opticalPathIdentifier);
        return { visibleOpticalPathIdentifiers };
      });
    } else {
      console.info(`hide optical path ${opticalPathIdentifier}`);
      this.volumeViewer.hideOpticalPath(opticalPathIdentifier);
      this.setState((state) => {
        const visibleOpticalPathIdentifiers = new Set(
          state.visibleOpticalPathIdentifiers
        );
        visibleOpticalPathIdentifiers.delete(opticalPathIdentifier);
        return { visibleOpticalPathIdentifiers };
      });
    }
  }

  /**
   * Handle change of optical path style.
   */
  handleOpticalPathStyleChange({
    opticalPathIdentifier,
    styleOptions,
  }: {
    opticalPathIdentifier: string;
    styleOptions: {
      opacity?: number;
      color?: number[];
      limitValues?: number[];
    };
  }): void {
    console.log(`change style of optical path ${opticalPathIdentifier}`);
    this.volumeViewer.setOpticalPathStyle(opticalPathIdentifier, styleOptions);
  }

  /**
   * Handle toggling of optical path activity, i.e., whether a given
   * optical path should be either added or removed from the viewport.
   */
  handleOpticalPathActivityChange({
    opticalPathIdentifier,
    isActive,
  }: {
    opticalPathIdentifier: string;
    isActive: boolean;
  }): void {
    console.log(`change activity of optical path ${opticalPathIdentifier}`);
    if (isActive) {
      console.info(`activate optical path ${opticalPathIdentifier}`);
      this.volumeViewer.activateOpticalPath(opticalPathIdentifier);
      this.setState((state) => {
        const activeOpticalPathIdentifiers = new Set(
          state.activeOpticalPathIdentifiers
        );
        activeOpticalPathIdentifiers.add(opticalPathIdentifier);
        return { activeOpticalPathIdentifiers };
      });
    } else {
      console.info(`deactivate optical path ${opticalPathIdentifier}`);
      this.volumeViewer.deactivateOpticalPath(opticalPathIdentifier);
      this.setState((state) => {
        const activeOpticalPathIdentifiers = new Set(
          state.activeOpticalPathIdentifiers
        );
        activeOpticalPathIdentifiers.delete(opticalPathIdentifier);
        return { activeOpticalPathIdentifiers };
      });
    }
  }

  /**
   * Set default presentation state that is either defined by metadata included
   * in the DICOM Slide Microscopy instance or by the viewer.
   */
  setDefaultPresentationState(): void {
    const visibleOpticalPathIdentifiers: Set<string> = new Set();
    const opticalPaths = this.volumeViewer.getAllOpticalPaths();
    opticalPaths.sort((a, b) => {
      if (a.identifier.localeCompare(b.identifier) === 1) {
        return 1;
      } else if (b.identifier.localeCompare(a.identifier) === 1) {
        return -1;
      }
      return 0;
    });
    opticalPaths.forEach((item: dmv.opticalPath.OpticalPath) => {
      const identifier = item.identifier;
      const style = this.volumeViewer.getOpticalPathDefaultStyle(identifier);
      this.volumeViewer.setOpticalPathStyle(identifier, style);
      this.volumeViewer.hideOpticalPath(identifier);
      this.volumeViewer.deactivateOpticalPath(identifier);
      if (item.isMonochromatic) {
        /*
         * If the image metadata contains a palette color lookup table for the
         * optical path, then it will be displayed by default.
         */
        if (item.paletteColorLookupTableUID != null) {
          visibleOpticalPathIdentifiers.add(identifier);
        }
      } else {
        /* Color images will always be displayed by default. */
        visibleOpticalPathIdentifiers.add(identifier);
      }
    });

    /*
     * If no optical paths have been selected for visualization so far, select
     * first n optical paths and set a default value of interest (VOI) window
     * (using pre-computed pixel data statistics) and a default color.
     */
    if (visibleOpticalPathIdentifiers.size === 0) {
      const defaultColors = [[255, 255, 255]];
      opticalPaths.forEach((item: dmv.opticalPath.OpticalPath) => {
        const identifier = item.identifier;
        if (item.isMonochromatic) {
          const numVisible = visibleOpticalPathIdentifiers.size;
          if (numVisible < defaultColors.length) {
            const style = {
              ...this.volumeViewer.getOpticalPathStyle(identifier),
            };
            const index = numVisible;
            style.color = defaultColors[index];
            const stats = this.state.pixelDataStatistics[item.identifier];
            if (stats != null) {
              style.limitValues = [stats.min, stats.max];
            }
            this.volumeViewer.setOpticalPathStyle(item.identifier, style);
            visibleOpticalPathIdentifiers.add(item.identifier);
          }
        }
      });
    }

    console.info(
      `selected n=${visibleOpticalPathIdentifiers.size} optical paths ` +
        "for visualization"
    );
    visibleOpticalPathIdentifiers.forEach((identifier) => {
      this.volumeViewer.showOpticalPath(identifier);
    });
    this.setState((state) => ({
      activeOpticalPathIdentifiers: new Set(visibleOpticalPathIdentifiers),
      visibleOpticalPathIdentifiers: new Set(visibleOpticalPathIdentifiers),
    }));
  }

  /**
   * Handler that gets called when a presentation state has been selected from
   * the current list of available presentation states.
   */
  handlePresentationStateReset(): void {
    this.setState({ selectedPresentationStateUID: undefined });
    const urlPath = this.props.location.pathname;
    this.props.navigate(urlPath);
    this.setDefaultPresentationState();
  }

  /**
   * Handler that gets called when a presentation state has been selected from
   * the current list of available presentation states.
   */
  handlePresentationStateSelection(value?: string, option?: any): void {
    if (value != null) {
      console.info(`select Presentation State instance "${value}"`);
      let presentationState;
      this.state.presentationStates.forEach((instance) => {
        if (instance.SOPInstanceUID === value) {
          presentationState = instance;
        }
      });
      if (presentationState != null) {
        let urlPath = this.props.location.pathname;
        urlPath += `?state=${value}`;
        this.props.navigate(urlPath);
        this.setPresentationState(presentationState);
      } else {
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        NotificationMiddleware.onError(
          NotificationMiddlewareContext.SLIM,
          new CustomError(
            errorTypes.VISUALIZATION,
            "Presentation State could not be found"
          )
        );
        console.log(
          "failed to handle section of presentation state: " +
            `could not find instance "${value}"`
        );
      }
    } else {
      this.handlePresentationStateReset();
    }
    this.setState({ selectedPresentationStateUID: value });
  }

  /**
   * Handler that will toggle the ROI drawing tool, i.e., either activate or
   * de-activate it, depending on its current state.
   */
  handleRoiDrawing(): void {
    if (this.state.isRoiDrawingActive) {
      console.info("deactivate drawing of ROIs");
      this.volumeViewer.deactivateDrawInteraction();
      this.volumeViewer.activateSelectInteraction({});
      this.setState({
        isAnnotationModalVisible: false,
        isSelectedRoiModalVisible: false,
        isRoiTranslationActive: false,
        isRoiDrawingActive: false,
        isRoiModificationActive: false,
        isGoToModalVisible: false,
      });
    } else {
      console.info("activate drawing of ROIs");
      this.setState({
        isAnnotationModalVisible: true,
        isSelectedRoiModalVisible: false,
        isRoiDrawingActive: true,
        isRoiModificationActive: false,
        isRoiTranslationActive: false,
        isGoToModalVisible: false,
      });
      this.volumeViewer.deactivateSelectInteraction();
      this.volumeViewer.deactivateSnapInteraction();
      this.volumeViewer.deactivateTranslateInteraction();
      this.volumeViewer.deactivateModifyInteraction();
    }
  }

  /**
   * Handler that will toggle the ROI modification tool, i.e., either activate
   * or de-activate it, depending on its current state.
   */
  handleRoiModification(): void {
    console.info("toggle modification of ROIs");
    if (this.volumeViewer.isModifyInteractionActive) {
      this.volumeViewer.deactivateModifyInteraction();
      this.volumeViewer.deactivateSnapInteraction();
      this.volumeViewer.activateSelectInteraction({});
      this.setState({
        isRoiTranslationActive: false,
        isRoiDrawingActive: false,
        isRoiModificationActive: false,
      });
    } else {
      this.setState({
        isRoiModificationActive: true,
        isRoiDrawingActive: false,
        isRoiTranslationActive: false,
      });
      this.volumeViewer.deactivateDrawInteraction();
      this.volumeViewer.deactivateTranslateInteraction();
      this.volumeViewer.deactivateSelectInteraction();
      this.volumeViewer.activateSnapInteraction({});
      this.volumeViewer.activateModifyInteraction({});
    }
  }

  /**
   * Handler that will toggle the ROI translation tool, i.e., either activate
   * or de-activate it, depending on its current state.
   */
  handleRoiTranslation(): void {
    console.info("toggle translation of ROIs");
    if (this.volumeViewer.isTranslateInteractionActive) {
      this.volumeViewer.deactivateTranslateInteraction();
      this.setState({
        isRoiTranslationActive: false,
        isRoiDrawingActive: false,
        isRoiModificationActive: false,
      });
    } else {
      this.setState({
        isRoiTranslationActive: true,
        isRoiDrawingActive: false,
        isRoiModificationActive: false,
      });
      this.volumeViewer.deactivateModifyInteraction();
      this.volumeViewer.deactivateSnapInteraction();
      this.volumeViewer.deactivateDrawInteraction();
      this.volumeViewer.deactivateSelectInteraction();
      this.volumeViewer.activateTranslateInteraction({});
    }
  }

  handleGoTo(): void {
    this.volumeViewer.deactivateDrawInteraction();
    this.volumeViewer.deactivateModifyInteraction();
    this.volumeViewer.deactivateSnapInteraction();
    this.volumeViewer.deactivateTranslateInteraction();
    this.volumeViewer.deactivateSelectInteraction();
    this.setState({
      isGoToModalVisible: true,
      isAnnotationModalVisible: false,
      isSelectedRoiModalVisible: false,
      isReportModalVisible: false,
      isRoiTranslationActive: false,
      isRoiModificationActive: false,
      isRoiDrawingActive: false,
    });
  }

  /**
   * Handler that will toggle the ROI removal tool, i.e., either activate
   * or de-activate it, depending on its current state.
   */
  handleRoiRemoval(): void {
    this.volumeViewer.deactivateDrawInteraction();
    this.volumeViewer.deactivateSnapInteraction();
    this.volumeViewer.deactivateTranslateInteraction();
    this.volumeViewer.deactivateModifyInteraction();
    if (this.state.selectedRoiUIDs.size > 0) {
      this.state.selectedRoiUIDs.forEach((uid) => {
        if (uid === undefined) {
          // eslint-disable-next-line @typescript-eslint/no-floating-promises
          message.warning("No annotation was selected for removal");
          return;
        }
        console.info(`remove ROI "${uid}"`);
        this.volumeViewer.removeROI(uid);
        // eslint-disable-next-line @typescript-eslint/no-floating-promises
        message.info("Annotation was removed");
      });
      this.setState({
        selectedRoiUIDs: new Set(),
        isRoiTranslationActive: false,
        isRoiDrawingActive: false,
        isRoiModificationActive: false,
      });
    } else {
      this.state.visibleRoiUIDs.forEach((uid) => {
        console.info(`remove ROI "${uid}"`);
        this.volumeViewer.removeROI(uid);
      });
      this.setState({
        visibleRoiUIDs: new Set(),
        isRoiTranslationActive: false,
        isRoiDrawingActive: false,
        isRoiModificationActive: false,
      });
    }
    this.volumeViewer.activateSelectInteraction({});
  }

  /**
   * Handler that will toggle the ROI visibility tool, i.e., either activate
   * or de-activate it, depending on its current state.
   */
  handleRoiVisibilityChange(): void {
    console.info("toggle visibility of ROIs");
    if (this.volumeViewer.areROIsVisible) {
      this.volumeViewer.deactivateDrawInteraction();
      this.volumeViewer.deactivateSnapInteraction();
      this.volumeViewer.deactivateTranslateInteraction();
      this.volumeViewer.deactivateSelectInteraction();
      this.volumeViewer.deactivateModifyInteraction();
      this.volumeViewer.hideROIs();
      this.setState({
        areRoisHidden: true,
        isRoiDrawingActive: false,
        isRoiModificationActive: false,
        isRoiTranslationActive: false,
      });
    } else {
      this.volumeViewer.showROIs();
      this.volumeViewer.activateSelectInteraction({});
      this.state.selectedRoiUIDs.forEach((uid) => {
        if (uid !== undefined) {
          this.volumeViewer.setROIStyle(uid, this.selectedRoiStyle);
        }
      });
      this.setState({ areRoisHidden: false });
    }
  }

  render(): React.ReactNode {
    const rois: dmv.roi.ROI[] = [];
    const segments: dmv.segment.Segment[] = [];
    const mappings: dmv.mapping.ParameterMapping[] = [];
    const annotationGroups: dmv.annotation.AnnotationGroup[] = [];
    rois.push(...this.volumeViewer.getAllROIs());
    segments.push(...this.volumeViewer.getAllSegments());
    mappings.push(...this.volumeViewer.getAllParameterMappings());
    annotationGroups.push(...this.volumeViewer.getAllAnnotationGroups());

    const openSubMenuItems = [
      "specimens",
      "optical-paths",
      "annotations",
      "presentation-states",
    ];

    let report: React.ReactNode;
    const dataset = this.state.generatedReport;
    if (dataset !== undefined) {
      report = <Report dataset={dataset} />;
    }

    let annotationMenuItems: React.ReactNode;
    if (rois.length > 0) {
      annotationMenuItems = (
        <AnnotationList
          rois={rois}
          selectedRoiUIDs={this.state.selectedRoiUIDs}
          visibleRoiUIDs={this.state.visibleRoiUIDs}
          onSelection={this.handleAnnotationSelection}
          onVisibilityChange={this.handleAnnotationVisibilityChange}
        />
      );
    }

    const findingOptions = this.findingOptions.map((finding) => {
      return (
        <Select.Option key={finding.CodeValue} value={finding.CodeValue}>
          {finding.CodeMeaning}
        </Select.Option>
      );
    });

    const geometryTypeOptionsMapping: { [key: string]: React.ReactNode } = {
      point: (
        <Select.Option key="point" value="point">
          Point
        </Select.Option>
      ),
      circle: (
        <Select.Option key="circle" value="circle">
          Circle
        </Select.Option>
      ),
      box: (
        <Select.Option key="box" value="box">
          Box
        </Select.Option>
      ),
      polygon: (
        <Select.Option key="polygon" value="polygon">
          Polygon
        </Select.Option>
      ),
      line: (
        <Select.Option key="line" value="line">
          Line
        </Select.Option>
      ),
      freehandpolygon: (
        <Select.Option key="freehandpolygon" value="freehandpolygon">
          Polygon (freehand)
        </Select.Option>
      ),
      freehandline: (
        <Select.Option key="freehandline" value="freehandline">
          Line (freehand)
        </Select.Option>
      ),
    };

    const annotationConfigurations: React.ReactNode[] = [
      <Select
        style={{ minWidth: 130 }}
        onSelect={this.handleAnnotationFindingSelection}
        key="annotation-finding"
        defaultActiveFirstOption
      >
        {findingOptions}
      </Select>,
    ];

    const selectedFinding = this.state.selectedFinding;
    if (selectedFinding !== undefined) {
      const key = _buildKey(selectedFinding);
      this.evaluationOptions[key].forEach((evaluation) => {
        const evaluationOptions = evaluation.values.map((code) => {
          return (
            <Select.Option
              key={code.CodeValue}
              value={code.CodeValue}
              label={evaluation.name}
            >
              {code.CodeMeaning}
            </Select.Option>
          );
        });
        annotationConfigurations.push(
          <>
            {evaluation.name.CodeMeaning}
            <Select
              style={{ minWidth: 130 }}
              onSelect={this.handleAnnotationEvaluationSelection}
              allowClear
              onClear={this.handleAnnotationEvaluationClearance}
              defaultActiveFirstOption={false}
            >
              {evaluationOptions}
            </Select>
          </>
        );
      });
      const geometryTypeOptions = this.geometryTypeOptions[key].map((name) => {
        return geometryTypeOptionsMapping[name];
      });
      annotationConfigurations.push(
        <>
          ROI geometry type
          <Select
            style={{ minWidth: 130 }}
            onSelect={this.handleAnnotationGeometryTypeSelection}
            key="annotation-geometry-type"
          >
            {geometryTypeOptions}
          </Select>
        </>
      );
      annotationConfigurations.push(
        <Checkbox
          onChange={this.handleAnnotationMeasurementActivation}
          key="annotation-measurement"
        >
          measure
        </Checkbox>
      );
    }

    const specimenMenu = (
      <Menu.SubMenu key="specimens" title="Specimens">
        <SpecimenList
          metadata={this.props.slide.volumeImages[0]}
          showstain={false}
        />
      </Menu.SubMenu>
    );

    const equipmentMenu = (
      <Menu.SubMenu key="equipment" title="Equipment">
        <Equipment metadata={this.props.slide.volumeImages[0]} />
      </Menu.SubMenu>
    );

    const opticalPaths = this.volumeViewer.getAllOpticalPaths();
    opticalPaths.sort((a, b) => {
      if (a.identifier.localeCompare(b.identifier) === 1) {
        return 1;
      } else if (b.identifier.localeCompare(a.identifier) === 1) {
        return -1;
      }
      return 0;
    });
    const opticalPathStyles: {
      [identifier: string]: {
        opacity: number;
        color?: number[];
        limitValues?: number[];
        paletteColorLookupTable?: dmv.color.PaletteColorLookupTable;
      };
    } = {};
    const opticalPathMetadata: {
      [identifier: string]: dmv.metadata.VLWholeSlideMicroscopyImage[];
    } = {};
    opticalPaths.forEach((opticalPath) => {
      const identifier = opticalPath.identifier;
      const metadata = this.volumeViewer.getOpticalPathMetadata(identifier);
      opticalPathMetadata[identifier] = metadata;
      const style = {
        ...this.volumeViewer.getOpticalPathStyle(identifier),
      };
      opticalPathStyles[identifier] = style;
    });
    const opticalPathMenu = (
      <Menu.SubMenu key="optical-paths" title="Optical Paths">
        <OpticalPathList
          metadata={opticalPathMetadata}
          opticalPaths={opticalPaths}
          defaultOpticalPathStyles={opticalPathStyles}
          visibleOpticalPathIdentifiers={
            this.state.visibleOpticalPathIdentifiers
          }
          activeOpticalPathIdentifiers={this.state.activeOpticalPathIdentifiers}
          onOpticalPathVisibilityChange={this.handleOpticalPathVisibilityChange}
          onOpticalPathStyleChange={this.handleOpticalPathStyleChange}
          onOpticalPathActivityChange={this.handleOpticalPathActivityChange}
          selectedPresentationStateUID={this.state.selectedPresentationStateUID}
        />
      </Menu.SubMenu>
    );

    let presentationStateMenu;
    if (this.state.presentationStates.length > 0) {
      const presentationStateOptions = [];
      this.state.presentationStates.forEach((instance) => {
        presentationStateOptions.push(
          <Select.Option
            key={instance.SOPInstanceUID}
            value={instance.SOPInstanceUID}
            dropdownMatchSelectWidth={false}
            size="small"
          >
            {instance.ContentDescription}
          </Select.Option>
        );
      });
      presentationStateOptions.push(
        <Select.Option
          key="default-presentation-state"
          value={null}
          dropdownMatchSelectWidth={false}
          size="small"
        >
          <></>
        </Select.Option>
      );
      presentationStateMenu = (
        <Menu.SubMenu key="presentation-states" title="Presentation States">
          <Space align="center" size={20} style={{ padding: "14px" }}>
            <Select
              style={{ minWidth: 200, maxWidth: 200 }}
              onSelect={this.handlePresentationStateSelection}
              key="presentation-states"
              value={this.state.selectedPresentationStateUID}
            >
              {presentationStateOptions}
            </Select>
            <Tooltip title="Reset">
              <Btn
                icon={<UndoOutlined />}
                type="primary"
                onClick={this.handlePresentationStateReset}
              />
            </Tooltip>
          </Space>
        </Menu.SubMenu>
      );
    }

    let segmentationMenu;
    if (segments.length > 0) {
      const defaultSegmentStyles: {
        [segmentUID: string]: {
          opacity: number;
        };
      } = {};
      const segmentMetadata: {
        [segmentUID: string]: dmv.metadata.Segmentation[];
      } = {};
      const segments = this.volumeViewer.getAllSegments();
      segments.forEach((segment) => {
        defaultSegmentStyles[segment.uid] = this.volumeViewer.getSegmentStyle(
          segment.uid
        );
        segmentMetadata[segment.uid] = this.volumeViewer.getSegmentMetadata(
          segment.uid
        );
      });
      segmentationMenu = (
        <Menu.SubMenu key="segmentations" title="Segmentations">
          <SegmentList
            segments={segments}
            metadata={segmentMetadata}
            defaultSegmentStyles={defaultSegmentStyles}
            visibleSegmentUIDs={this.state.visibleSegmentUIDs}
            onSegmentVisibilityChange={this.handleSegmentVisibilityChange}
            onSegmentStyleChange={this.handleSegmentStyleChange}
          />
        </Menu.SubMenu>
      );
      openSubMenuItems.push("segmentations");
    }

    let parametricMapMenu;
    if (mappings.length > 0) {
      const defaultMappingStyles: {
        [mappingUID: string]: {
          opacity: number;
        };
      } = {};
      const mappingMetadata: {
        [mappingUID: string]: dmv.metadata.ParametricMap[];
      } = {};
      mappings.forEach((mapping) => {
        defaultMappingStyles[mapping.uid] =
          this.volumeViewer.getParameterMappingStyle(mapping.uid);
        mappingMetadata[mapping.uid] =
          this.volumeViewer.getParameterMappingMetadata(mapping.uid);
      });
      parametricMapMenu = (
        <Menu.SubMenu key="parmetric-maps" title="Parametric Maps">
          <MappingList
            mappings={mappings}
            metadata={mappingMetadata}
            defaultMappingStyles={defaultMappingStyles}
            visibleMappingUIDs={this.state.visibleMappingUIDs}
            onMappingVisibilityChange={this.handleMappingVisibilityChange}
            onMappingStyleChange={this.handleMappingStyleChange}
          />
        </Menu.SubMenu>
      );
      openSubMenuItems.push("parametric-maps");
    }

    let annotationGroupMenu;
    if (annotationGroups.length > 0) {
      const defaultAnnotationGroupStyles: {
        [annotationGroupUID: string]: {
          opacity: number;
          color: number[];
        };
      } = {};
      const annotationGroupMetadata: {
        [
          annotationGroupUID: string
        ]: dmv.metadata.MicroscopyBulkSimpleAnnotations;
      } = {};
      const annotationGroups = this.volumeViewer.getAllAnnotationGroups();
      annotationGroups.forEach((annotationGroup) => {
        defaultAnnotationGroupStyles[annotationGroup.uid] =
          this.volumeViewer.getAnnotationGroupStyle(annotationGroup.uid);
        annotationGroupMetadata[annotationGroup.uid] =
          this.volumeViewer.getAnnotationGroupMetadata(annotationGroup.uid);
      });
      annotationGroupMenu = (
        <Menu.SubMenu key="annotation-groups" title="Annotation Groups">
          <AnnotationGroupList
            annotationGroups={annotationGroups}
            metadata={annotationGroupMetadata}
            defaultAnnotationGroupStyles={defaultAnnotationGroupStyles}
            visibleAnnotationGroupUIDs={this.state.visibleAnnotationGroupUIDs}
            onAnnotationGroupVisibilityChange={
              this.handleAnnotationGroupVisibilityChange
            }
            onAnnotationGroupStyleChange={this.handleAnnotationGroupStyleChange}
          />
        </Menu.SubMenu>
      );
      openSubMenuItems.push("annotationGroups");
    }

    let toolbar;
    let toolbarHeight = "0px";
    const annotationTools = [
      <ToolbarButton
        tooltip="Draw ROI [Alt+D]"
        icon={FaDrawPolygon}
        onClick={this.handleRoiDrawing}
        isSelected={this.state.isRoiDrawingActive}
        key="draw-roi-button"
        disabled={true}
      />,
      <ToolbarButton
        tooltip="Modify ROIs [Alt+M]"
        icon={FaHandPointer}
        onClick={this.handleRoiModification}
        isSelected={this.state.isRoiModificationActive}
        key="modify-roi-button"
        disabled={true}
      />,
      <ToolbarButton
        tooltip="Translate ROIs [Alt+T]"
        icon={FaHandPaper}
        onClick={this.handleRoiTranslation}
        isSelected={this.state.isRoiTranslationActive}
        key="translate-roi-button"
        disabled={true}
      />,
      <ToolbarButton
        tooltip="Remove selected ROI [Alt+R]"
        onClick={this.handleRoiRemoval}
        icon={FaTrash}
        key="remove-roi-button"
        disabled={true}
      />,
      <ToolbarButton
        tooltip="Show/Hide ROIs [Alt+V]"
        icon={this.state.areRoisHidden ? FaEye : FaEyeSlash}
        onClick={this.handleRoiVisibilityChange}
        isSelected={this.state.areRoisHidden}
        key="toggle-roi-visibility-button"
        disabled={true}
      />,
      <ToolbarButton
        tooltip="Save ROIs [Alt+S]"
        icon={FaSave}
        onClick={this.handleReportGeneration}
        key="generate-report-button"
        disabled={true}
      />,
    ];
    const controlTools = [
      <ToolbarButton
        tooltip="Go to [Alt+G]"
        icon={FaCrosshairs}
        onClick={this.handleGoTo}
        key="go-to-slide-position-button"
        disabled={true}
      />,
    ];
    if (this.props.enableAnnotationTools) {
      toolbar = (
        <Space direction="horizontal" style={{ lineHeight: 0 }}>
          {annotationTools.map((item, i) => {
            return <React.Fragment key={i}>{item}</React.Fragment>;
          })}
          {controlTools.map((item, i) => {
            return <React.Fragment key={i}>{item}</React.Fragment>;
          })}
          <div style={{ fontSize: "medium" }}> Quick zoom </div>
          {/**
           * ICON CHO CÓ,K CẦN CHỨC NĂNG
           */}
          <ToolbarButton
            tooltip="160x"
            onClick={() => {
              alert("zoom 160x");
            }}
            svgIcon={
              <svg
                width="37"
                height="12"
                viewBox="0 0 37 12"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M5.16174 0.818182V11H2.70577V3.11506H2.64611L0.369124 4.5071V2.37926L2.87978 0.818182H5.16174ZM12.1943 11.1392C11.6375 11.1392 11.1039 11.0497 10.5934 10.8707C10.083 10.6884 9.62896 10.4001 9.23123 10.0057C8.83351 9.60795 8.5203 9.08759 8.2916 8.4446C8.06291 7.7983 7.95022 7.01113 7.95353 6.0831C7.95685 5.24455 8.05959 4.49219 8.26177 3.82599C8.46395 3.15649 8.7523 2.58807 9.12683 2.12074C9.50467 1.65341 9.95543 1.29711 10.4791 1.05185C11.0061 0.803267 11.5944 0.678977 12.244 0.678977C12.9566 0.678977 13.5847 0.818182 14.1283 1.09659C14.6751 1.37169 15.1126 1.7429 15.4408 2.21023C15.7689 2.67424 15.9628 3.19129 16.0224 3.76136H13.6013C13.5283 3.43987 13.3676 3.19626 13.119 3.03054C12.8738 2.86151 12.5821 2.77699 12.244 2.77699C11.6209 2.77699 11.1552 3.04711 10.847 3.58736C10.5421 4.1276 10.3863 4.85014 10.3797 5.75497H10.4443C10.5835 5.45005 10.784 5.18821 11.0459 4.96946C11.3077 4.75071 11.6077 4.58333 11.9457 4.46733C12.2871 4.34801 12.6484 4.28835 13.0295 4.28835C13.6394 4.28835 14.178 4.42921 14.6453 4.71094C15.1126 4.99266 15.4789 5.37879 15.744 5.86932C16.0092 6.35653 16.1401 6.91501 16.1368 7.54474C16.1401 8.25402 15.9744 8.88044 15.6396 9.42401C15.3049 9.96425 14.8408 10.3852 14.2476 10.6868C13.6576 10.9884 12.9732 11.1392 12.1943 11.1392ZM12.1794 9.25C12.481 9.25 12.7511 9.17874 12.9898 9.03622C13.2284 8.8937 13.4157 8.69981 13.5515 8.45455C13.6874 8.20928 13.7537 7.93253 13.7504 7.62429C13.7537 7.31274 13.6874 7.03598 13.5515 6.79403C13.419 6.55208 13.2334 6.35985 12.9947 6.21733C12.7594 6.07481 12.4893 6.00355 12.1844 6.00355C11.9623 6.00355 11.7551 6.04498 11.5629 6.12784C11.3707 6.2107 11.2033 6.3267 11.0608 6.47585C10.9216 6.62169 10.8122 6.79403 10.7327 6.9929C10.6531 7.18845 10.6117 7.40057 10.6084 7.62926C10.6117 7.93087 10.6813 8.20431 10.8172 8.44957C10.9531 8.69484 11.1387 8.89039 11.374 9.03622C11.6093 9.17874 11.8778 9.25 12.1794 9.25ZM22.4435 11.2486C21.5552 11.2486 20.7896 11.0381 20.1466 10.6172C19.5036 10.1929 19.0081 9.58475 18.6601 8.79261C18.3121 7.99716 18.1397 7.04096 18.1431 5.92401C18.1464 4.80705 18.3204 3.85914 18.6651 3.08026C19.0131 2.29806 19.5069 1.70312 20.1466 1.29545C20.7896 0.884469 21.5552 0.678977 22.4435 0.678977C23.3317 0.678977 24.0974 0.884469 24.7404 1.29545C25.3867 1.70312 25.8838 2.29806 26.2318 3.08026C26.5798 3.86245 26.7522 4.81037 26.7489 5.92401C26.7489 7.04427 26.5749 8.00213 26.2269 8.79759C25.8789 9.59304 25.3834 10.2012 24.7404 10.6222C24.1007 11.0398 23.3351 11.2486 22.4435 11.2486ZM22.4435 9.23509C22.9738 9.23509 23.403 8.96496 23.7311 8.42472C24.0593 7.88116 24.2217 7.04759 24.2183 5.92401C24.2183 5.18821 24.1438 4.58168 23.9946 4.1044C23.8455 3.62382 23.6383 3.26586 23.3732 3.03054C23.108 2.79522 22.7981 2.67756 22.4435 2.67756C21.9165 2.67756 21.4906 2.94437 21.1658 3.47798C20.841 4.00829 20.6769 4.82363 20.6736 5.92401C20.6703 6.66974 20.7415 7.28788 20.8874 7.77841C21.0365 8.26894 21.2453 8.63518 21.5138 8.87713C21.7823 9.11577 22.0922 9.23509 22.4435 9.23509ZM30.9464 3.36364L32.2042 5.87926L33.5068 3.36364H35.9578L33.8249 7.18182L36.0373 11H33.6062L32.2042 8.46449L30.837 11H28.3711L30.5884 7.18182L28.4805 3.36364H30.9464Z"
                  fill="#a15151"
                />
              </svg>
            }
            key="generate-report-button"
          />
          <ToolbarButton
            tooltip="80x"
            onClick={() => {
              alert("zoom 80x");
            }}
            svgIcon={
              <svg
                width="29"
                height="12"
                viewBox="0 0 29 12"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M4.2399 11.1392C3.44776 11.1392 2.74013 11.0133 2.11703 10.7614C1.49724 10.5062 1.01002 10.1615 0.65538 9.72727C0.304054 9.28977 0.128392 8.79593 0.128392 8.24574C0.128392 7.82481 0.229481 7.43868 0.431659 7.08736C0.633837 6.73603 0.907274 6.44437 1.25197 6.21236C1.59998 5.97704 1.98777 5.82623 2.41532 5.75994V5.6804C1.85519 5.57765 1.39615 5.31747 1.03819 4.89986C0.683553 4.48224 0.506232 3.99337 0.506232 3.43324C0.506232 2.90294 0.666981 2.43063 0.988477 2.01633C1.31329 1.60204 1.75576 1.27557 2.31589 1.03693C2.87934 0.798295 3.52067 0.678977 4.2399 0.678977C4.95912 0.678977 5.5988 0.798295 6.15893 1.03693C6.72238 1.27557 7.16485 1.60204 7.48635 2.01633C7.81116 2.43063 7.97356 2.90294 7.97356 3.43324C7.97356 3.99669 7.79293 4.48722 7.43166 4.90483C7.0737 5.31913 6.61797 5.57765 6.06447 5.6804V5.75994C6.48871 5.82623 6.87318 5.97704 7.21788 6.21236C7.56589 6.44437 7.84099 6.73603 8.04316 7.08736C8.24866 7.43868 8.3514 7.82481 8.3514 8.24574C8.3514 8.79593 8.17408 9.28977 7.81944 9.72727C7.4648 10.1615 6.97759 10.5062 6.3578 10.7614C5.74132 11.0133 5.03535 11.1392 4.2399 11.1392ZM4.2399 9.39418C4.54151 9.39418 4.80666 9.33783 5.03535 9.22514C5.26404 9.10914 5.44302 8.94839 5.57228 8.7429C5.70486 8.53741 5.77115 8.3054 5.77115 8.04688C5.77115 7.78172 5.70486 7.5464 5.57228 7.34091C5.43971 7.13542 5.25742 6.97467 5.02541 6.85866C4.79672 6.73935 4.53488 6.67969 4.2399 6.67969C3.94823 6.67969 3.68639 6.73935 3.45439 6.85866C3.22238 6.97467 3.04009 7.13542 2.90751 7.34091C2.77494 7.5464 2.70865 7.78172 2.70865 8.04688C2.70865 8.3054 2.77328 8.53741 2.90254 8.7429C3.03512 8.94508 3.21575 9.10417 3.44444 9.22017C3.67645 9.33617 3.9416 9.39418 4.2399 9.39418ZM4.2399 4.9446C4.49842 4.9446 4.72711 4.89157 4.92598 4.78551C5.12815 4.67945 5.28559 4.53196 5.39828 4.34304C5.51428 4.15412 5.57228 3.94034 5.57228 3.7017C5.57228 3.46307 5.51428 3.2526 5.39828 3.07031C5.28559 2.88802 5.12981 2.7455 4.93095 2.64276C4.73208 2.53669 4.50173 2.48366 4.2399 2.48366C3.98137 2.48366 3.75102 2.53669 3.54885 2.64276C3.34667 2.7455 3.18923 2.88802 3.07655 3.07031C2.96386 3.2526 2.90751 3.46307 2.90751 3.7017C2.90751 3.94034 2.96386 4.15412 3.07655 4.34304C3.19255 4.52865 3.35164 4.67614 3.55382 4.78551C3.756 4.89157 3.98469 4.9446 4.2399 4.9446ZM14.6345 11.2486C13.7462 11.2486 12.9806 11.0381 12.3376 10.6172C11.6946 10.1929 11.1991 9.58475 10.8511 8.79261C10.5031 7.99716 10.3308 7.04096 10.3341 5.92401C10.3374 4.80705 10.5114 3.85914 10.8561 3.08026C11.2041 2.29806 11.6979 1.70312 12.3376 1.29545C12.9806 0.884469 13.7462 0.678977 14.6345 0.678977C15.5228 0.678977 16.2884 0.884469 16.9314 1.29545C17.5777 1.70312 18.0748 2.29806 18.4229 3.08026C18.7709 3.86245 18.9432 4.81037 18.9399 5.92401C18.9399 7.04427 18.7659 8.00213 18.4179 8.79759C18.0699 9.59304 17.5744 10.2012 16.9314 10.6222C16.2917 11.0398 15.5261 11.2486 14.6345 11.2486ZM14.6345 9.23509C15.1648 9.23509 15.594 8.96496 15.9221 8.42472C16.2503 7.88116 16.4127 7.04759 16.4094 5.92401C16.4094 5.18821 16.3348 4.58168 16.1856 4.1044C16.0365 3.62382 15.8293 3.26586 15.5642 3.03054C15.299 2.79522 14.9891 2.67756 14.6345 2.67756C14.1075 2.67756 13.6816 2.94437 13.3568 3.47798C13.032 4.00829 12.8679 4.82363 12.8646 5.92401C12.8613 6.66974 12.9326 7.28788 13.0784 7.77841C13.2275 8.26894 13.4363 8.63518 13.7048 8.87713C13.9733 9.11577 14.2832 9.23509 14.6345 9.23509ZM23.1374 3.36364L24.3952 5.87926L25.6978 3.36364H28.1488L26.016 7.18182L28.2283 11H25.7972L24.3952 8.46449L23.028 11H20.5621L22.7795 7.18182L20.6715 3.36364H23.1374Z"
                  fill="#962525"
                />
              </svg>
            }
            key="generate-report-button"
          />
          <ToolbarButton
            tooltip="40x"
            onClick={() => {
              alert("zoom 40x");
            }}
            svgIcon={
              <svg
                width="29"
                height="12"
                viewBox="0 0 29 12"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0.0426316 9.30966V7.39062L4.2138 0.818182H5.91905V3.4233H4.92971L2.48865 7.29119V7.37074H8.52914V9.30966H0.0426316ZM4.95954 11V8.72301L5.00925 7.88281V0.818182H7.3111V11H4.95954ZM14.7302 11.2486C13.8419 11.2486 13.0763 11.0381 12.4333 10.6172C11.7903 10.1929 11.2948 9.58475 10.9468 8.79261C10.5988 7.99716 10.4265 7.04096 10.4298 5.92401C10.4331 4.80705 10.6071 3.85914 10.9518 3.08026C11.2998 2.29806 11.7936 1.70312 12.4333 1.29545C13.0763 0.884469 13.8419 0.678977 14.7302 0.678977C15.6185 0.678977 16.3841 0.884469 17.0271 1.29545C17.6734 1.70312 18.1705 2.29806 18.5186 3.08026C18.8666 3.86245 19.0389 4.81037 19.0356 5.92401C19.0356 7.04427 18.8616 8.00213 18.5136 8.79759C18.1656 9.59304 17.6701 10.2012 17.0271 10.6222C16.3874 11.0398 15.6218 11.2486 14.7302 11.2486ZM14.7302 9.23509C15.2605 9.23509 15.6897 8.96496 16.0178 8.42472C16.346 7.88116 16.5084 7.04759 16.5051 5.92401C16.5051 5.18821 16.4305 4.58168 16.2813 4.1044C16.1322 3.62382 15.925 3.26586 15.6599 3.03054C15.3947 2.79522 15.0848 2.67756 14.7302 2.67756C14.2032 2.67756 13.7773 2.94437 13.4525 3.47798C13.1277 4.00829 12.9636 4.82363 12.9603 5.92401C12.957 6.66974 13.0283 7.28788 13.1741 7.77841C13.3232 8.26894 13.532 8.63518 13.8005 8.87713C14.069 9.11577 14.3789 9.23509 14.7302 9.23509ZM23.2331 3.36364L24.4909 5.87926L25.7935 3.36364H28.2445L26.1117 7.18182L28.324 11H25.8929L24.4909 8.46449L23.1237 11H20.6578L22.8752 7.18182L20.7672 3.36364H23.2331Z"
                  fill="#72a82a"
                />
              </svg>
            }
            key="generate-report-button"
          />
          <ToolbarButton
            tooltip="20x"
            onClick={() => {
              alert("zoom 20x");
            }}
            svgIcon={
              <svg
                width="28"
                height="12"
                viewBox="0 0 28 12"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0.529226 11V9.23011L4.243 5.97869C4.52141 5.7268 4.75839 5.49645 4.95394 5.28764C5.14949 5.07552 5.29864 4.8634 5.40139 4.65128C5.50413 4.43584 5.5555 4.20218 5.5555 3.95028C5.5555 3.66856 5.49419 3.42827 5.37156 3.2294C5.24892 3.02723 5.07989 2.87145 4.86445 2.76207C4.64902 2.6527 4.4021 2.59801 4.12369 2.59801C3.84196 2.59801 3.59504 2.65601 3.38292 2.77202C3.1708 2.88471 3.00508 3.04877 2.88576 3.2642C2.76976 3.47964 2.71175 3.74148 2.71175 4.04972H0.380078C0.380078 3.35701 0.535855 2.75876 0.847408 2.25497C1.15896 1.75118 1.59646 1.3634 2.15991 1.09162C2.72667 0.816524 3.38458 0.678977 4.13363 0.678977C4.90588 0.678977 5.57705 0.808238 6.14712 1.06676C6.7172 1.32528 7.15801 1.68655 7.46957 2.15057C7.78443 2.61127 7.94187 3.14654 7.94187 3.75639C7.94187 4.14418 7.86398 4.52865 7.7082 4.9098C7.55243 5.29096 7.27236 5.71188 6.868 6.17259C6.46696 6.63329 5.89689 7.18513 5.15778 7.82812L3.93974 8.9517V9.01634H8.06616V11H0.529226ZM14.3679 11.2486C13.4796 11.2486 12.714 11.0381 12.071 10.6172C11.428 10.1929 10.9325 9.58475 10.5845 8.79261C10.2365 7.99716 10.0642 7.04096 10.0675 5.92401C10.0708 4.80705 10.2448 3.85914 10.5895 3.08026C10.9375 2.29806 11.4313 1.70312 12.071 1.29545C12.714 0.884469 13.4796 0.678977 14.3679 0.678977C15.2562 0.678977 16.0218 0.884469 16.6648 1.29545C17.3111 1.70312 17.8082 2.29806 18.1562 3.08026C18.5043 3.86245 18.6766 4.81037 18.6733 5.92401C18.6733 7.04427 18.4993 8.00213 18.1513 8.79759C17.8033 9.59304 17.3078 10.2012 16.6648 10.6222C16.0251 11.0398 15.2595 11.2486 14.3679 11.2486ZM14.3679 9.23509C14.8982 9.23509 15.3274 8.96496 15.6555 8.42472C15.9837 7.88116 16.1461 7.04759 16.1428 5.92401C16.1428 5.18821 16.0682 4.58168 15.919 4.1044C15.7699 3.62382 15.5627 3.26586 15.2976 3.03054C15.0324 2.79522 14.7225 2.67756 14.3679 2.67756C13.8409 2.67756 13.415 2.94437 13.0902 3.47798C12.7654 4.00829 12.6013 4.82363 12.598 5.92401C12.5947 6.66974 12.666 7.28788 12.8118 7.77841C12.9609 8.26894 13.1697 8.63518 13.4382 8.87713C13.7067 9.11577 14.0166 9.23509 14.3679 9.23509ZM22.8708 3.36364L24.1286 5.87926L25.4312 3.36364H27.8822L25.7494 7.18182L27.9617 11H25.5306L24.1286 8.46449L22.7614 11H20.2955L22.5129 7.18182L20.4049 3.36364H22.8708Z"
                  fill="#5d68c5"
                />
              </svg>
            }
            key="generate-report-button"
          />
          <ToolbarButton
            tooltip="10x"
            onClick={() => {
              alert("zoom 10x");
            }}
            svgIcon={
              <svg
                width="26"
                height="12"
                viewBox="0 0 26 12"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M5.2217 0.818182V11H2.76573V3.11506H2.70607L0.429084 4.5071V2.37926L2.93974 0.818182H5.2217ZM12.3835 11.2486C11.4953 11.2486 10.7296 11.0381 10.0866 10.6172C9.44366 10.1929 8.94815 9.58475 8.60014 8.79261C8.25213 7.99716 8.07978 7.04096 8.0831 5.92401C8.08641 4.80705 8.26042 3.85914 8.60511 3.08026C8.95313 2.29806 9.44697 1.70312 10.0866 1.29545C10.7296 0.884469 11.4953 0.678977 12.3835 0.678977C13.2718 0.678977 14.0374 0.884469 14.6804 1.29545C15.3267 1.70312 15.8239 2.29806 16.1719 3.08026C16.5199 3.86245 16.6922 4.81037 16.6889 5.92401C16.6889 7.04427 16.5149 8.00213 16.1669 8.79759C15.8189 9.59304 15.3234 10.2012 14.6804 10.6222C14.0407 11.0398 13.2751 11.2486 12.3835 11.2486ZM12.3835 9.23509C12.9138 9.23509 13.343 8.96496 13.6712 8.42472C13.9993 7.88116 14.1617 7.04759 14.1584 5.92401C14.1584 5.18821 14.0838 4.58168 13.9347 4.1044C13.7855 3.62382 13.5784 3.26586 13.3132 3.03054C13.0481 2.79522 12.7382 2.67756 12.3835 2.67756C11.8565 2.67756 11.4306 2.94437 11.1058 3.47798C10.781 4.00829 10.617 4.82363 10.6136 5.92401C10.6103 6.66974 10.6816 7.28788 10.8274 7.77841C10.9766 8.26894 11.1854 8.63518 11.4538 8.87713C11.7223 9.11577 12.0322 9.23509 12.3835 9.23509ZM20.8864 3.36364L22.1442 5.87926L23.4468 3.36364H25.8978L23.765 7.18182L25.9773 11H23.5462L22.1442 8.46449L20.7771 11H18.3112L20.5285 7.18182L18.4205 3.36364H20.8864Z"
                  fill="#a12b49"
                />
              </svg>
            }
            key="generate-report-button"
          />
          <ToolbarButton
            tooltip="5x"
            onClick={() => {
              alert("zoom 5x");
            }}
            svgIcon={
              <svg
                width="18"
                height="11"
                viewBox="0 0 18 11"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M4.60312 11.1392C3.8607 11.1392 3.20114 11.005 2.62443 10.7365C2.04773 10.468 1.592 10.0985 1.25724 9.62784C0.925805 9.1572 0.753456 8.61695 0.740199 8.0071H3.12656C3.14645 8.38163 3.29891 8.68324 3.58395 8.91193C3.86899 9.13731 4.20871 9.25 4.60312 9.25C4.91136 9.25 5.1848 9.18205 5.42344 9.04616C5.66207 8.91027 5.84934 8.7197 5.98523 8.47443C6.12112 8.22585 6.18741 7.94081 6.18409 7.61932C6.18741 7.29119 6.11946 7.0045 5.98026 6.75923C5.84437 6.51397 5.65544 6.32339 5.41349 6.1875C5.17486 6.0483 4.89976 5.97869 4.58821 5.97869C4.29323 5.97538 4.01316 6.04001 3.74801 6.17259C3.48617 6.30516 3.28731 6.4858 3.15142 6.71449L0.978835 6.30682L1.42131 0.818182H7.95398V2.80185H3.44474L3.21108 5.20312H3.27074C3.43977 4.9214 3.71155 4.68939 4.08608 4.5071C4.46392 4.3215 4.89313 4.22869 5.37372 4.22869C5.98357 4.22869 6.52713 4.37121 7.0044 4.65625C7.48499 4.93797 7.86283 5.32907 8.13793 5.82955C8.41634 6.33002 8.55554 6.90341 8.55554 7.54972C8.55554 8.24905 8.38982 8.86884 8.05838 9.40909C7.73026 9.94934 7.26955 10.3736 6.67628 10.6818C6.08632 10.9867 5.39527 11.1392 4.60312 11.1392ZM12.6946 3.36364L13.9524 5.87926L15.255 3.36364H17.706L15.5732 7.18182L17.7855 11H15.3544L13.9524 8.46449L12.5853 11H10.1194L12.3367 7.18182L10.2287 3.36364H12.6946Z"
                  fill="#17da78"
                />
              </svg>
            }
            key="generate-report-button"
          />
          <ToolbarButton
            tooltip="2x"
            onClick={() => {
              alert("zoom 2x");
            }}
            svgIcon={
              <svg
                width="18"
                height="11"
                viewBox="0 0 18 11"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M0.842116 11V9.23011L4.55589 5.97869C4.8343 5.7268 5.07128 5.49645 5.26683 5.28764C5.46238 5.07552 5.61153 4.8634 5.71428 4.65128C5.81702 4.43584 5.86839 4.20218 5.86839 3.95028C5.86839 3.66856 5.80708 3.42827 5.68445 3.2294C5.56181 3.02723 5.39278 2.87145 5.17734 2.76207C4.96191 2.6527 4.71499 2.59801 4.43658 2.59801C4.15485 2.59801 3.90793 2.65601 3.69581 2.77202C3.48369 2.88471 3.31797 3.04877 3.19865 3.2642C3.08265 3.47964 3.02464 3.74148 3.02464 4.04972H0.692968C0.692968 3.35701 0.848745 2.75876 1.1603 2.25497C1.47185 1.75118 1.90935 1.3634 2.4728 1.09162C3.03956 0.816524 3.69747 0.678977 4.44652 0.678977C5.21877 0.678977 5.88994 0.808238 6.46001 1.06676C7.03009 1.32528 7.4709 1.68655 7.78246 2.15057C8.09732 2.61127 8.25476 3.14654 8.25476 3.75639C8.25476 4.14418 8.17687 4.52865 8.02109 4.9098C7.86532 5.29096 7.58525 5.71188 7.18089 6.17259C6.77985 6.63329 6.20978 7.18513 5.47067 7.82812L4.25263 8.9517V9.01634H8.37905V11H0.842116ZM12.5579 3.36364L13.8157 5.87926L15.1183 3.36364H17.5693L15.4365 7.18182L17.6488 11H15.2177L13.8157 8.46449L12.4485 11H9.98263L12.2 7.18182L10.092 3.36364H12.5579Z"
                  fill="#8c84d4"
                />
              </svg>
            }
            key="generate-report-button"
          />
          <ToolbarButton
            tooltip="1x"
            onClick={() => {
              alert("zoom 1x");
            }}
            svgIcon={
              <svg
                width="16"
                height="11"
                viewBox="0 0 16 11"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M5.53459 0.818182V11H3.07862V3.11506H3.01896L0.741974 4.5071V2.37926L3.25263 0.818182H5.53459ZM10.5735 3.36364L11.8314 5.87926L13.1339 3.36364H15.5849L13.4521 7.18182L15.6645 11H13.2333L11.8314 8.46449L10.4642 11H7.99826L10.2156 7.18182L8.10763 3.36364H10.5735Z"
                  fill="#fff"
                />
              </svg>
            }
            key="generate-report-button"
          />
        </Space>
      );
    }

    let cursor = "default";
    if (this.state.isLoading) {
      cursor = "progress";
    }

    let selectedRoiInformation;
    if (this.state.selectedRoi != null) {
      const roiAttributes: Array<{
        name: string;
        value: string;
        unit?: string;
      }> = [
        {
          name: "UID",
          value: this.state.selectedRoi.uid,
        },
      ];
      const roiScoordAttributes: Array<{
        name: string;
        value: string;
      }> = [
        {
          name: "Graphic type",
          value: this.state.selectedRoi.scoord3d.graphicType,
        },
      ];
      const roiEvaluationAttributes: Array<{
        name: string;
        value: string;
      }> = [];
      this.state.selectedRoi.evaluations.forEach((item) => {
        if (item.ValueType === "CODE") {
          const codeItem = item as dcmjs.sr.valueTypes.CodeContentItem;
          roiEvaluationAttributes.push({
            name: codeItem.ConceptNameCodeSequence[0].CodeMeaning,
            value: codeItem.ConceptCodeSequence[0].CodeMeaning,
          });
        } else {
          const textItem = item as dcmjs.sr.valueTypes.TextContentItem;
          roiEvaluationAttributes.push({
            name: textItem.ConceptNameCodeSequence[0].CodeMeaning,
            value: textItem.TextValue,
          });
        }
      });
      const roiMeasurmentAttributesPerOpticalPath: {
        [identifier: string]: Array<{
          name: string;
          value: string;
          unit?: string;
        }>;
      } = {};
      this.state.selectedRoi.measurements.forEach((item) => {
        let identifier = "default";
        if (item.ContentSequence != null) {
          const refItems = findContentItemsByName({
            content: item.ContentSequence,
            name: new dcmjs.sr.coding.CodedConcept({
              value: "121112",
              meaning: "Source of Measurement",
              schemeDesignator: "DCM",
            }),
          });
          if (refItems.length > 0) {
            identifier =
              // @ts-expect-error
              refItems[0].ReferencedSOPSequence[0]
                .ReferencedOpticalPathIdentifier;
          }
        }
        if (!(identifier in roiMeasurmentAttributesPerOpticalPath)) {
          roiMeasurmentAttributesPerOpticalPath[identifier] = [];
        }
        const measuredValueItem = item.MeasuredValueSequence[0];
        roiMeasurmentAttributesPerOpticalPath[identifier].push({
          name: item.ConceptNameCodeSequence[0].CodeMeaning,
          value: measuredValueItem.NumericValue.toString(),
          unit: measuredValueItem.MeasurementUnitsCodeSequence[0].CodeMeaning,
        });
      });
      const createRoiDescription = (
        attributes: Array<{ name: string; value: string; unit?: string }>
      ): React.ReactNode[] => {
        return attributes.map((item) => {
          let value;
          if (item.unit != null) {
            value = `${item.value} [${item.unit}]`;
          } else {
            value = item.value;
          }
          return (
            <Descriptions.Item key={item.name} label={item.name}>
              {value}
            </Descriptions.Item>
          );
        });
      };
      const roiDescriptions = createRoiDescription(roiAttributes);
      const roiScoordDescriptions = createRoiDescription(roiScoordAttributes);
      const roiEvaluationDescriptions = createRoiDescription(
        roiEvaluationAttributes
      );
      const roiMeasurementDescriptions = [];
      for (const identifier in roiMeasurmentAttributesPerOpticalPath) {
        const descriptions = createRoiDescription(
          roiMeasurmentAttributesPerOpticalPath[identifier]
        );
        if (identifier === "default") {
          roiMeasurementDescriptions.push(descriptions);
        } else {
          roiMeasurementDescriptions.push(
            <>
              <Divider orientation="left" orientationMargin={0} dashed plain>
                {identifier}
              </Divider>
              {descriptions}
            </>
          );
        }
      }
      selectedRoiInformation = (
        <>
          <Descriptions layout="horizontal" column={1}>
            {roiDescriptions}
          </Descriptions>
          <Divider orientation="left" orientationMargin={0}>
            Spatial coordinates
          </Divider>
          <Descriptions layout="horizontal" column={1}>
            {roiScoordDescriptions}
          </Descriptions>
          <Divider orientation="left" orientationMargin={0}>
            Evaluations
          </Divider>
          <Descriptions layout="horizontal" column={1}>
            {roiEvaluationDescriptions}
          </Descriptions>
          <Divider orientation="left" orientationMargin={0}>
            Measurements
          </Divider>
          <Descriptions layout="horizontal" column={1}>
            {roiMeasurementDescriptions}
          </Descriptions>
        </>
      );
    }

    return (
      <Layout style={{ height: "100%" }}>
        <Header app={this.props.appInfo} Toolbar={toolbar} />
        <Layout.Content>
          <Layout style={{ height: "100%" }} hasSider>
            <Layout.Sider
              width={300}
              style={{
                height: "100%",
                borderRight: "solid",
                borderRightWidth: 0.25,
                overflow: "hidden",
                background: "none",
              }}
            >
              {this.props.LayoutSider}
            </Layout.Sider>
            <Layout.Content style={{ height: "100%" }}>
              <div
                style={{
                  height: "100%",
                  overflow: "hidden",
                  cursor: cursor,
                }}
                ref={this.volumeViewportRef}
              />

              <Modal
                visible={this.state.isAnnotationModalVisible}
                title="Configure annotations"
                onOk={this.handleAnnotationConfigurationCompletion}
                onCancel={this.handleAnnotationConfigurationCancellation}
                okText="Select"
              >
                <Space align="start" direction="vertical">
                  {annotationConfigurations}
                </Space>
              </Modal>

              <Modal
                visible={this.state.isSelectedRoiModalVisible}
                title="Selected ROI"
                onCancel={this.handleRoiSelectionCancellation}
                maskClosable
                footer={null}
              >
                <Space align="start" direction="vertical">
                  {selectedRoiInformation}
                </Space>
              </Modal>

              <Modal
                visible={this.state.isGoToModalVisible}
                title="Go to slide position"
                onOk={this.handleSlidePositionSelection}
                onCancel={this.handleSlidePositionSelectionCancellation}
                okText="Select"
              >
                <Space align="start" direction="vertical">
                  <InputNumber
                    placeholder={
                      "[" +
                      `${this.state.validXCoordinateRange[0]}` +
                      ", " +
                      `${this.state.validXCoordinateRange[1]}` +
                      "]"
                    }
                    prefix="X Coordinate [mm]"
                    onChange={this.handleXCoordinateSelection}
                    onPressEnter={this.handleXCoordinateSelection}
                    controls={false}
                    addonAfter={
                      this.state.isSelectedXCoordinateValid ? (
                        <CheckOutlined style={{ color: "rgba(0,0,0,.45)" }} />
                      ) : (
                        <StopOutlined style={{ color: "rgba(0,0,0,.45)" }} />
                      )
                    }
                  />
                  <InputNumber
                    placeholder={
                      "[" +
                      `${this.state.validYCoordinateRange[0]}` +
                      ", " +
                      `${this.state.validYCoordinateRange[1]}` +
                      "]"
                    }
                    prefix="Y Coordinate [mm]"
                    onChange={this.handleYCoordinateSelection}
                    onPressEnter={this.handleYCoordinateSelection}
                    controls={false}
                    addonAfter={
                      this.state.isSelectedYCoordinateValid ? (
                        <CheckOutlined style={{ color: "rgba(0,0,0,.45)" }} />
                      ) : (
                        <StopOutlined style={{ color: "rgba(0,0,0,.45)" }} />
                      )
                    }
                  />
                  <InputNumber
                    placeholder="[0 - 40]"
                    prefix="Magnification"
                    onChange={this.handleMagnificationSelection}
                    onPressEnter={this.handleMagnificationSelection}
                    controls={false}
                    addonAfter={
                      this.state.isSelectedMagnificationValid ? (
                        <CheckOutlined style={{ color: "rgba(0,0,0,.45)" }} />
                      ) : (
                        <StopOutlined style={{ color: "rgba(0,0,0,.45)" }} />
                      )
                    }
                  />
                </Space>
              </Modal>

              <Modal
                visible={this.state.isReportModalVisible}
                title="Verify and save report"
                onOk={this.handleReportVerification}
                onCancel={this.handleReportCancellation}
                okText="Save"
              >
                {report}
              </Modal>
            </Layout.Content>

            {/* <Layout.Sider
          width={300}
          reverseArrow
          style={{
            borderLeft: "solid",
            borderLeftWidth: 0.25,
            overflow: "hidden",
            background: "none",
          }}
        >
          <Menu
            mode="inline"
            defaultOpenKeys={openSubMenuItems}
            style={{ height: "100%" }}
            inlineIndent={14}
            forceSubMenuRender
            onOpenChange={() => {
              // Give menu item time to render before updating viewer size
              setTimeout(() => {
                if (this.labelViewer != null) {
                  this.labelViewer.resize();
                }
              }, 100);
            }}
          >
            <Menu.SubMenu key="label" title="Slide label">
              <Menu.Item style={{ height: "100%" }} key="image">
                <div style={{ height: "220px" }} ref={this.labelViewportRef} />
              </Menu.Item>
            </Menu.SubMenu>
            {specimenMenu}
            {equipmentMenu}
            {opticalPathMenu}
            {presentationStateMenu}
            <Menu.SubMenu key="annotations" title="Annotations">
              {annotationMenuItems}
            </Menu.SubMenu>
            {annotationGroupMenu}
            {segmentationMenu}
            {parametricMapMenu}
          </Menu>
        </Layout.Sider> */}
          </Layout>
        </Layout.Content>
      </Layout>
    );
  }
}

export default withRouter(SlideViewer);
