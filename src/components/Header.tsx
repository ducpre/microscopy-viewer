import React from "react";
import { Col, Layout, Row } from "antd";

import { RouteComponentProps, withRouter } from "../utils/router";

interface HeaderProps extends RouteComponentProps {
  Toolbar: JSX.Element;
}

/**
 * React component for the application header.
 */
class Header extends React.Component<HeaderProps> {
  constructor(props: HeaderProps) {
    super(props);
  }

  render(): React.ReactNode {
    return (
      <>
        <Layout.Header
          style={{
            width: "100%",
            padding: "0 16px",
            background: "#27272b",
            height: "48px",
            lineHeight: 0,
          }}
        >
          <Row style={{ height: "100%", padding: "4px 0" }}>
            {/* <Col>
              <Space align="center" direction="horizontal">
                <img
                  src={itech_logo_with_title}
                  alt=""
                  style={{ height: "54px" }}
                />
              </Space>
            </Col> */}
            <Col flex="auto">{this.props.Toolbar}</Col>
          </Row>
        </Layout.Header>
      </>
    );
  }
}

export default withRouter(Header);
