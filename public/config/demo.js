
const WINDOW_ORIGIN_TOKEN = '<origin>';
const preprocessAPIEndPoints = (input) => {
  if (!input) return '';
  else {
    if (input.includes(WINDOW_ORIGIN_TOKEN))
      return input.replace(WINDOW_ORIGIN_TOKEN, window.origin);
    return input;
  }
};

window.config = {
  path: '/',
  servers: [
    {
      id: 'demo',
      url: `${preprocessAPIEndPoints('<origin>/dcm4chee-arc/aets/DCM4CHEE/rs')}`,
      write: false
    }
  ],
  preload: true,
  disableAnnotationTools: false,
  annotations: [
    {
      finding: { value: '85756007', schemeDesignator: 'SCT', meaning: 'Tissue' }
    }
  ]
}
