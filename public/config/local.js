


const WINDOW_ORIGIN_TOKEN = '<origin>';

const preprocessAPIEndPoints = (input) => {
  if (!input) return '';
  else {
    if (input.includes(WINDOW_ORIGIN_TOKEN))
      return input.replace(WINDOW_ORIGIN_TOKEN, window.origin);
    return input;
  }
};


window.config = {
  // This must match the location configured for web server
  path: '/',
  servers: [
    {
      id: 'local',
      // This must match the proxy location configured for the web server
      url: 'http://192.168.1.32:18080/dcm4chee-arc/aets/DCM4CHEE/rs',
      // url: `${preprocessAPIEndPoints('<origin>/dcm4chee/dcm4chee-arc/aets/DCM4CHEE/rs')}`,
      // url: 'http://127.0.0.1:80/orthanc/wado-rs',
      write: false
    }
  ],
  disableWorklist: false,
  disableAnnotationTools: false,
  mode: 'dark',
  preload: true,
  annotations: [
    {
      finding: {
        value: '85756007',
        schemeDesignator: 'SCT',
        meaning: 'Tissue'
      },
      findingCategory: {
        value: '91723000',
        schemeDesignator: 'SCT',
        meaning: 'Anatomical structure'
      },
      geometryTypes: ['polygon', 'freehandpolygon'],
      style: {
        stroke: {
          color: [255, 255, 0, 1],
          width: 2
        },
        fill: {
          color: [255, 255, 255, 0.2]
        }
      },
    },
    {
      finding: {
        value: '108369006',
        schemeDesignator: 'SCT',
        meaning: 'Tumor'
      },
      findingCategory: {
        value: '49755003',
        schemeDesignator: 'SCT',
        meaning: 'Morphologically abnormal structure'
      },
      geometryTypes: ['polygon', 'freehandpolygon'],
      style: {
        stroke: {
          color: [255, 0, 255, 1],
          width: 2
        },
        fill: {
          color: [255, 255, 255, 0.2]
        }
      }
    },
    {
      finding: {
        value: '34823008',
        schemeDesignator: 'SCT',
        meaning: 'Tumor necrosis'
      },
      findingCategory: {
        value: '49755003',
        schemeDesignator: 'SCT',
        meaning: 'Morphologically abnormal structure'
      },
      geometryTypes: ['polygon', 'freehandpolygon'],
      style: {
        stroke: {
          color: [51, 204, 51, 1],
          width: 2
        },
        fill: {
          color: [255, 255, 255, 0.2]
        }
      }
    },
    {
      finding: {
        value: '369705002',
        schemeDesignator: 'SCT',
        meaning: 'Invasive tumor border'
      },
      findingCategory: {
        value: '395557000',
        schemeDesignator: 'SCT',
        meaning: 'Tumor finding'
      },
      geometryTypes: ['line', 'freehandline'],
      style: {
        stroke: {
          color: [51, 102, 255, 1],
          width: 2
        },
        fill: {
          color: [255, 255, 255, 0.2]
        }
      }
    },
    {
      finding: {
        value: '399721002',
        schemeDesignator: 'SCT',
        meaning: 'Tumor infiltration by lymphocytes present'
      },
      findingCategory: {
        value: '395557000',
        schemeDesignator: 'SCT',
        meaning: 'Tumor finding'
      },
      geometryTypes: ['polygon', 'freehandpolygon'],
      style: {
        stroke: {
          color: [51, 204, 204, 1],
          width: 2
        },
        fill: {
          color: [255, 255, 255, 0.2]
        }
      }
    },
    {
      finding: {
        value: '47973001',
        schemeDesignator: 'SCT',
        meaning: 'Artifact'
      },
      geometryTypes: ['polygon', 'freehandpolygon'],
      style: {
        stroke: {
          color: [255, 80, 80, 1],
          width: 2
        },
        fill: {
          color: [255, 255, 255, 0.2]
        }
      }
    }
  ]
}


// http://192.168.1.32:18080/dcm4chee-arc/aets/DCM4CHEE/rs/studies/1.3.6.1.4.1.36533.198105154197118787424114551732359412216366/series/1.3.6.1.4.1.36533.811454139167246642501471411651085384157105
